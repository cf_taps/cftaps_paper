source("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/figs/code/functions.r")
library(readr)
library(tidyverse)
library(reshape2)
library(dplyr)
library(ggplot2)
library(ggpubr)
library(pryr)
library(pheatmap)
library(factoextra)
library(FactoMineR)
library(RColorBrewer)

# Fig 2A Plot clinical statistics ----
setwd("/users/ludwig/cfo155/cfo155/cfDNA/cfDNA_clean/cftaps_paper/plots/")
dat <- read.table("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/resource/cfDNA_cohort_1_sample_list_11302020.excured.newid.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)

seldat <- dat[dat$Status %in% c("Ctrl","HCC","PDAC"),]
seldat$Status <- factor(seldat$Status, levels=c("PDAC","HCC","Ctrl"))

stage <- read.table("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/resource/cfDNA_cohort_1_sample_list_stage.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)
stage$new <- gsub("A|B","",stage$Stage)

cancer_stage <- table(merge(seldat%>%select(Sample_ID,Status),stage%>%select("Sample.ID","new"),by.x="Sample_ID",by.y="Sample.ID") %>% select(Status,new)) %>% as.data.frame() 

dat1 <- cancer_stage[cancer_stage$Status %in% "HCC",]
dat1$fraction <- dat1$Freq / sum(dat1$Freq)
dat1$ymax <- cumsum(dat1$fraction)
dat1$ymin <- c(0, head(dat1$ymax, n=-1))
dat1$labelPosition <- (dat1$ymax + dat1$ymin) / 2


dat2 <- cancer_stage[cancer_stage$Status %in% "PDAC",]
dat2$fraction <- dat2$Freq / sum(dat2$Freq)
dat2$ymax <- cumsum(dat2$fraction)
dat2$ymin <- c(0, head(dat2$ymax, n=-1))
dat2$labelPosition <- (dat2$ymax + dat2$ymin) / 2

dat <- rbind(dat1[,1:3],dat2[,1:3])
dat$Status <- factor(dat$Status, levels=c("HCC","PDAC"))
p1 <- ggplot(dat, aes(x=Status,y=Freq,fill=new)) +
  geom_bar(position="stack", stat="identity") +
  scale_fill_manual(values=rev(brewer.pal(4,"Spectral")))  +
  mytheme + ylab("Number of patients")
ggsave("cohort_info.pdf",p1,width=3,height=3)

# Fig 2C-D PCA ----

plot_pca <- function(dat, scale=FALSE, sel_pc, smp_status, outputlabel, selcolor ){
  res_pca <- prcomp(t(dat), scale = scale)
  p1 <- fviz_pca_ind(
    X = res_pca,
    axes = c(sel_pc),
    habillage = smp_status,
    #addEllipses = TRUE,
    mean.point = FALSE,
    repel = TRUE,
    label = "none",
    pointsize = 3
  )+labs(title = outputlabel) + 
    scale_color_manual(values=selcolor) +
    theme(panel.grid.major = element_blank(), 
          panel.grid.minor = element_blank(),
          panel.background = element_blank())
  ggsave(paste0(outputlabel, ".label.pdf"),p1,width=5,height=5)
}

methFile <-  "/users/ludwig/cfo155/cfo155/cfDNA/cfDNA_clean/cftaps_paper/data/filtered.selsmp.ws1000.agg.count.cpgnum.depth2.ratio.txt"

ratios <- read_delim(methFile, delim = "\t", col_names = TRUE) %>% as.data.frame(stringsAsFactors=FALSE)
colnames(ratios) <- gsub("Healthy","Ctrl",colnames(ratios))
ratios <- ratios[,-grep("HCC_NHCCKAMI035",colnames(ratios))]
## remove cured sample ##

selid <- "Ctrl_|PDAC_"
selsmp <- ratios[,grep(selid,colnames(ratios))]
smpstatus <- unlist(lapply(colnames(selsmp),function(x)str_split(x,pattern="_")[[1]][1]))
outfix<- gsub(".*/|agg.count.cpgnum.","",gsub("txt",gsub("\\||_$","",selid),methFile))
plot_pca(selsmp, scale=FALSE, sel_pc <- c(1,2), smp_status=smpstatus, outputlabel= paste0(outfix,".excured.pca"),selcolor=c("grey","#0073C2FF"))

selid <- "Ctrl_|HCC_"
selsmp <- ratios[,grep(selid,colnames(ratios))]
smpstatus <- unlist(lapply(colnames(selsmp),function(x)str_split(x,pattern="_")[[1]][1]))
outfix<- gsub(".*/|agg.count.cpgnum.","",gsub("txt",gsub("\\||_$","",selid),methFile))
plot_pca(selsmp, scale=FALSE, sel_pc <- c(1,2), smp_status=smpstatus, outputlabel= paste0(outfix,".excured.pca"),selcolor=c("grey","#A73030FF"))

# Fig 2E Genomic features ----

enrichfile <- "/users/ludwig/cfo155/cfo155/cfDNA/cfDNA_clean/cftaps_paper/data/pc_hg38_allfeatures.enrich.top200.notmerge.sel.txt"
dat <- read.table(enrichfile,header=FALSE,sep="\t")
colnames(dat) <- c("smp","regions","filter","numregion","left","right","two_tail","ratio")
dat$logp <- -log(dat$two_tail,10)
dat <- dat[dat$regions!="hg38_cpg_inter" & dat$regions!= "hg38_genes_promoters" & dat$numregion!=0, ]
dat$smp <- gsub("filtered.selsmp.ws1000.agg.count.cpgnum.depth2.ratio.|.xls","",dat$smp)
dat$regions <- gsub("features/|.3col|.bed","",dat$regions)


seldat <- dat[dat$numregion==200 , ]
p <- ggplot(seldat,aes(x=regions,y=logp,fill=smp)) + 
  geom_bar(stat = "identity", width=0.8) +
  mytheme + 
  scale_fill_manual(values=c("#A73030FF","#0073C2FF")) +
  theme(legend.position = "none") +
  facet_grid(. ~ smp, scales="free") + coord_flip()

ggsave(gsub(".txt","top200.logp.pdf",basename(enrichfile)), p, width=6, height=3)

# Fig 2F-I, Fig S3B,E (methylation Predictions) ----
## predictions see fig2.dmr_loo.r
source("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/figs/code/functions.r")
scorefile <- "/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/meth_correction/exclude_cured/2class_models/enhancer.count.agg.count.cpgnum.depth2.HCC_excured.add_Cirrhosis.loo.pval0.002.ori.score.txt"
smps <- read.table("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/resource/cfDNA_cohort_1_sample_list_11302020.excured.newid.txt",header = TRUE, sep="\t",stringsAsFactors = FALSE)
cancer <-"HCC"
addtest <- "Cirrhosis"

scores <- read.table(scorefile,header=TRUE)
scores <- merge(scores,smps[,c(1,10)],by.x="smp",by.y="Sample_ID")
colnames(scores)[4] <- "id"
scores$id <- gsub("trl_|CC_|DAC_","",scores$id)
fac <- with(scores, reorder(id, pred_test_cvglm, median, order = TRUE))
scores$id <- factor(scores$id, levels = levels(fac))
scores$status <- factor(scores$status, levels=c("Healthy",cancer, addtest))
scores$truth <- as.numeric(scores$status==cancer)
mlauc1 <- round(AUC(scores[scores$status!=addtest,]$pred_test_cvglm, scores[scores$status!=addtest,]$truth),2)
roc_c <- roc(truth ~ pred_test_cvglm,  scores[scores$status!=addtest,], percent=TRUE)
cutoff <- coords(roc_c, "best", ret=c("threshold","specificity"), transpose = FALSE)
p1 <- ggplot() +
  geom_roc(data=scores[scores$status!=addtest,], aes(d = truth, m = pred_test_cvglm), labels = FALSE, color="#A73030FF") +
  theme_classic() +
  annotate(geom="text", x=0.3, y=0.6, label=paste0(cancer," AUC: ",mlauc1),color="#A73030FF",size=5) +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"))
scores$label <- "loo"
scores$label[scores$status==addtest] <- "add"

p2 <- ggplot(scores[scores$status!=addtest,]) + 
  geom_point(aes(x=id, y=pred_test_cvglm, color=status)) +
  facet_grid(~label, scales = "free_y")+ylim(0,1)+
  scale_color_manual(values=c("grey","#A73030FF")) +
  geom_hline(yintercept = cutoff$threshold, linetype =3) + mytheme
scores <- scores[order(scores$pred_test_cvglm),]
rownames(scores) <- make.names(scores$smp, unique = TRUE, allow_ = TRUE)
fac <- with(scores, reorder(id, pred_test_cvglm, median, order = TRUE))
scores$id <- factor(scores$id, levels = levels(fac))

scores$idx <- as.numeric(str_split_fixed(rownames(scores),"\\.",2)[,2])+1
scores$idx[is.na(scores$idx)] <-1
sumscores <- summarySE(scores[scores$status==addtest,],measurevar = "pred_test_cvglm",groupvars = "id")
sumscores$N <- sumscores$N/2
selscores <- merge(scores[scores$status==addtest,],sumscores,by="id")
p3 <- ggplot(selscores) + 
  geom_point(aes(x=idx, y=pred_test_cvglm.x, color=status),size=0.8,alpha=0.8) + 
  geom_pointrange(aes(x=N, y=median, ymin=median-sd, ymax=median+sd),color="grey30") +
  facet_grid(~id, scales = "free_y")+ylim(0,1)+
  scale_color_manual(values="#629bb9") + geom_hline(yintercept = cutoff$threshold, linetype =3) +
  mytheme + theme(legend.position = "none")
p <- plot_grid(p1, p2 + theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)),p3, 
               label_size = 12, nrow=1,rel_widths = c(0.7,1.5,0.6))
ggsave(gsub("txt",paste0(addtest,".pdf"),basename(scorefile)),p,width=12,height=3)
cat(cutoff$threshold,"\n")


scorefile <- "/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/meth_correction/exclude_cured/2class_models/promoter.count.agg.count.cpgnum.depth2.PDAC_excured.add_Pancreatitis.loo.pval0.002.ori.score.txt"
cancer <-"PDAC"
addtest <- "Pancreatitis"

scores <- read.table(scorefile,header=TRUE)
scores <- merge(scores,smps[,c(1,10)],by.x="smp",by.y="Sample_ID")
colnames(scores)[4] <- "id"
scores$id <- gsub("trl_|CC_|DAC_","",scores$id)
fac <- with(scores, reorder(id, pred_test_cvglm, median, order = TRUE))
scores$id <- factor(scores$id, levels = levels(fac))
scores$status <- factor(scores$status, levels=c("Healthy",cancer, addtest))
scores$truth <- as.numeric(scores$status==cancer)
mlauc1 <- round(AUC(scores[scores$status!=addtest,]$pred_test_cvglm, scores[scores$status!=addtest,]$truth),2)
roc_c <- roc(truth ~ pred_test_cvglm,  scores[scores$status!=addtest,], percent=TRUE)
cutoff <- coords(roc_c, "best", ret=c("threshold","specificity"), transpose = FALSE)
p1 <- ggplot() +
  geom_roc(data=scores[scores$status!=addtest,], aes(d = truth, m = pred_test_cvglm), labels = FALSE, color="#0073C2FF") +
  theme_classic() +
  annotate(geom="text", x=0.3, y=0.6, label=paste0(cancer," AUC: ",mlauc1),color="#0073C2FF",size=5) +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"))
scores$label <- "loo"
scores$label[scores$status==addtest] <- "add"
p2 <- ggplot(scores[scores$status!=addtest,], aes(x=id, y=pred_test_cvglm, color=status)) + 
  geom_point() +
  facet_grid(~label, scales = "free_y")+ylim(0,1)+
  scale_color_manual(values=c("grey","#0073C2FF")) + 
  geom_hline(yintercept = cutoff$threshold, linetype =3) + mytheme
scores <- scores[order(scores$pred_test_cvglm),]
rownames(scores) <- make.names(scores$smp, unique = TRUE, allow_ = TRUE)
fac <- with(scores, reorder(id, pred_test_cvglm, median, order = TRUE))
scores$id <- factor(scores$id, levels = levels(fac))

scores$idx <- as.numeric(str_split_fixed(rownames(scores),"\\.",2)[,2])+1
scores$idx[is.na(scores$idx)] <-1
sumscores <- summarySE(scores[scores$status==addtest,],measurevar = "pred_test_cvglm",groupvars = "id")
sumscores$N <- sumscores$N/2
selscores <- merge(scores[scores$status==addtest,],sumscores,by="id")
p3 <- ggplot(selscores) + 
  geom_point(aes(x=idx, y=pred_test_cvglm.x, color=status),size=0.8,alpha=0.8) + 
  geom_pointrange(aes(x=N, y=median, ymin=median-sd, ymax=median+sd),color="grey30") +
  facet_grid(~id, scales = "free_y")+ylim(0,1) +
  scale_color_manual(values="#a08c41") + geom_hline(yintercept = cutoff$threshold, linetype =3) +
  mytheme + theme(legend.position = "none") 
p <- plot_grid(p1,p2+theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)),p3, label_size = 12, nrow=1,rel_widths = c(0.7,1.5,0.6))
ggsave(gsub("txt",paste0(addtest,".pdf"),basename(scorefile)),p,width=12,height=3)
cat(cutoff$threshold,"\n")


# Fig S3H (predict on cfWGBS) ----

setwd("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/revision/")
source("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/figs/code/functions.r")
scorefile <- "cftaps_wgbs.enhancer.count.agg.count.cpgnum.depth2allfilter.p0.002.ori.score.txt"
smps <- read.table("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/resource/cfDNA_cohort_1_sample_list_11302020.excured.newid.txt",header = TRUE, sep="\t",stringsAsFactors = FALSE)
cancer <-"HCC"
addtest <- "Cirrhosis;wgbs"

scores <- read.table(scorefile,header=TRUE,stringsAsFactors = FALSE)
scores <- merge(scores,smps[,c(1,10)],by.x="smp",by.y="Sample_ID",all.x=TRUE)
scores$idx[is.na(scores$idx)] <- scores$smp[is.na(scores$idx)]
colnames(scores)[4] <- "id"

fac <- with(scores, reorder(id, pred_test_cvglm, median, order = TRUE))
scores$id <- factor(scores$id, levels = levels(fac))
scores$status <- factor(scores$status, levels=c("Healthy",cancer, addtest))
scores$truth <- as.numeric(scores$status==cancer)
mlauc1 <- round(AUC(scores[scores$status%in%c("HCC","Healthy"),]$pred_test_cvglm, 
                    scores[scores$status%in%c("HCC","Healthy"),]$truth),2)
roc_c <- roc(truth ~ pred_test_cvglm,  scores[scores$status%in%c("HCC","Healthy"),], percent=TRUE)
cutoff <- coords(roc_c, "best", ret=c("threshold","specificity"), transpose = FALSE)

scores <- scores[order(scores$pred_test_cvglm),]
rownames(scores) <- make.names(scores$smp, unique = TRUE, allow_ = TRUE)
fac <- with(scores, reorder(id, pred_test_cvglm, median, order = TRUE))
scores$id <- factor(scores$id, levels = levels(fac))

scores$idx <- as.numeric(str_split_fixed(rownames(scores),"\\.",2)[,2])+1
scores$idx[is.na(scores$idx)] <-1
sumscores <- summarySE(scores[!scores$status%in%c("HCC","Healthy"),],measurevar = "pred_test_cvglm",groupvars = "id")
sumscores$N <- sumscores$N/2

selscores <- merge(scores[!scores$status%in%c("HCC","Healthy"),],sumscores,by="id")
selscores$label[grep("Cirrhosis",selscores$id)] <- "Cirrhosis"
selscores$label[grep("^N",selscores$id)] <- "normal"
selscores$label[grep("^L",selscores$id)] <- "livercancer"

p4 <- ggplot(selscores[!selscores$label%in%c("Cirrhosis"),]) + 
  geom_point(aes(x=idx, y=pred_test_cvglm.x, color=label),size=0.8,alpha=0.8) + 
  geom_pointrange(aes(x=N, y=median, ymin=median-sd, ymax=median+sd),color="grey30") +
  facet_grid(~id +label, scales = "free_y") +
  scale_color_manual(values=c("#A73030FF","grey")) + geom_hline(yintercept = cutoff$threshold, linetype =3) +
  mytheme + theme(legend.position = "none") +
  ylim(0,1.02)
ggsave(gsub("txt",paste0(gsub(";",".",addtest),".pdf"),scorefile),p4,width=7,height=3)
cat(cutoff$threshold,"\n")



