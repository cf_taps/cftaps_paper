setwd("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/cnv_v2")
.libPaths(c(.libPaths(),"/gpfs2/well/ludwig/users/cfo155/miniconda2/lib/R/library/"))
library(Smisc)
library(QDNAseq)
library(reshape2)
library(dplyr)
library(ggplot2)
library(stringr)
library(pheatmap)
# load data ----
dat <- list()
poss <- list()
prefix <- "ws100k.QDNAseq.rData$"
for(i in list.files(pattern=prefix)){
    rm(copyNumbersSmooth)
    load(i)
    dat[gsub(prefix,"",i)] <- do.call("rbind", lapply(copyNumbersSmooth@assayData, as.data.frame))
    copyNumbersta <- do.call("cbind", lapply(copyNumbersSmooth@assayData, as.data.frame))     
    pos <- as.data.frame(do.call("cbind",copyNumbersSmooth@featureData@data)) [which(copyNumbersta[,1] <= 0.8 | copyNumbersta[,1] >= 1.2),] 
    pos <- paste(paste0(pos[,1],":",paste0(pos[,2],"-",pos[,3])),collapse=";")
    poss[gsub(prefix,"",i)] <- pos
}
dat <- do.call("cbind", dat)
rownames(dat) <- do.call("rbind", lapply(copyNumbersSmooth@assayData, as.data.frame)) %>% rownames()
dat <- t(dat)
dat <- dat[,grep("X|Y",colnames(dat),invert=TRUE)]

sta <- apply(dat,1,function(x){sum(x>=1.2|x<=0.8,na.rm=TRUE)}) %>% as.data.frame(); sta$smp <- rownames(sta)
sta[order(sta$.),]

## filter smp which have cnv > 500 kb 
# write.table(do.call(rbind,poss) %>% as.data.frame(), quote = FALSE, sep = "\t", paste0(prefix,".selpos.txt"))
# write.table(do.call("cbind",copyNumbersSmooth@featureData@data) %>% as.data.frame(), quote = FALSE, row.names=FALSE, sep = "\t", paste0(prefix,".allpos.txt"))
# tail -n +2 ws100k.QDNAseq.rData\$.selpos.txt |grep -v ":-"|\
#     awk 'BEGIN{FS="\t";OFS="\t"}{gsub(";","\n"$1"\t",$2);print $0}'|\
#     awk 'BEGIN{OFS="\t"}{print $2,$1}'|sort|    sed 's/^/chr/g;s/:/\t/g;s/-/\t/g'|\
#     intersectBed -a - -b <(cat hg38_centromere.bed sv_repeat_telomere_centromere.bed hg38-blacklist.v2.bed |cut -f1-3) -v -nonamecheck|\
#     sort -k1,1 -k2,2n|awk 'BEGIN{OFS="\t"}{print $1,$2,$3+1,$4}'|bedtools merge -i - -c 4 -o collapse| awk '$3-$2>=500000'|\
#     cut -f4|sort -u|sed 's/,/\n/g'|sort -u|sed 's/^/"/g;s/$/",/g'|tr -d '\n'
#     cut -f4|sort -u|sed 's/,/\n/g'|sort -u|cut -d '_' -f2|tr '.' '|'|tr -d '\n'


dat.s <- apply(dat,1,function(x){movAvg2(x,bw=2)}) %>% t()
selsmp <- c("HCC_NHCCDAHA038.","HCC_NHCCDEHA060.","HCC_NHCCLEHO053.","HCC_NHCCLIMC055.","PDAC_PT15.","PDAC_PT25.","PDAC_PT31.")
apply(dat.s[rownames(dat.s) %in% selsmp,],1,function(x){quantile(x,na.rm=TRUE,probs=seq(0,1,1/10))})
apply(dat[rownames(dat) %in% selsmp,],1,function(x){quantile(x,na.rm=TRUE,probs=seq(0,1,1/10))})

# Figure S5A ----
smp <- read.table("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/resource/cfDNA_cohort_1_sample_list_11302020.excured.newid.txt",header=TRUE,sep="\t")
seldat <- dat;rownames(seldat)<- gsub(".*_|\\.","",rownames(seldat))
seldat <- merge(smp[,c(1,10)],seldat,by.x="Sample_ID",by.y=0)
rownames(seldat) <- seldat$idx

seldat<- seldat[order(as.numeric(gsub("_.*","",gsub("PDAC","4", gsub("HCC","3", gsub("Pancreatitis","2", gsub("Cirrhosis","1", gsub("Ctrl","0",seldat$idx))))))),as.numeric(gsub(".*_","",seldat$idx))),]
seldat[,1:2] <- NULL
annotation_col = data.frame(chr = gsub("copynumber.","chr",str_split_fixed(colnames(seldat),":",2)[,1])); rownames(annotation_col) = colnames(seldat)
annotation_col$chr <- factor(annotation_col$chr,levels=paste0("chr",seq(1,22)))
annotation_row = data.frame(status = str_split_fixed(rownames(seldat),"_",2)[,1]); rownames(annotation_row) = rownames(seldat)


pheatmap(log(seldat,2), annotation_col = annotation_col, annotation_row = annotation_row, cluster_rows=FALSE, cluster_cols=FALSE, 
    show_rownames = TRUE,show_colnames=FALSE, color=colorRampPalette(rev(brewer.pal(9,"RdBu")))(9), breaks=seq(-0.5,0.5,1/9), 
    filename=paste0(prefix,".log2.png"), width=13, height=10)

# Figure S5B ----
seldat <- dat[rownames(dat)%in%selsmp,] %>% t() %>% melt()
seldat$color <- cut(seldat$value,breaks=c(-Inf, 0.75,1.25,Inf))
seldat$chr <- gsub("copynumber.","chr",stringr::str_split_fixed(seldat$Var1,":",2)[,1])
seldat$chr <- factor(seldat$chr,levels=paste0("chr",seq(1,22)))
seldat$pos <- str_split_fixed(str_split_fixed(seldat$Var1,":",2)[,2],"-",2)[,1] %>% as.character %>% as.numeric()
seldat$value <- log(seldat$value,2)
seldat <- seldat[order(seldat$chr,seldat$pos),]
seldat$Var1 <- factor(seldat$Var1, levels=seldat$Var1[seq(1,nrow(seldat),length(selsmp))])
seldat$Var2 <- gsub(".*_|\\.","",seldat$Var2)
seldat <- merge(seldat,smp[,c(1,10)],by.x="Var2",by.y="Sample_ID")
p <- ggplot(seldat,aes(x=Var1,y=value,color=chr)) + geom_point(size=0.2) + facet_grid(rows=vars(idx)) + 
    theme(axis.text.x = element_blank(), legend.position = "none") + 
    scale_color_manual(values=rep(c("#A73030","#0073C2"),11)) + ylim(-1,1) + 
    geom_hline(yintercept=log(0.8,2), linetype="dashed", color = "grey",lwd = 1) + 
    geom_hline(yintercept=log(1.2,2), linetype="dashed", color = "grey",lwd = 1)
ggsave(p, filename=paste0(prefix,".log2.sel.png"), width=10,height=6)


