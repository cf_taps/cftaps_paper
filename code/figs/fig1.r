source("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/figs/code/functions.r")
library(stringr)
library(readr)
library(ggplot2)
setwd("/users/ludwig/cfo155/cfo155/cfDNA/cfDNA_clean/cftaps_paper/plots")
# Fig1b-c Plot sequencing statistics ----
dat1 <- read_delim("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/resource/cfDNA_cohort_1_sample_list_all_sta.txt",delim ="\t",col_names = TRUE)
dat2 <- read_delim("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/test/all_sta.all.xls",delim ="\t",col_names = TRUE)
dat2$sample[is.na(dat2$sample)] <- dat2$id[is.na(dat2$sample)]
seldat <- rbind(dat2[dat2$id%in%c("PT52","GI4284"),c(10,2:9)],dat1[dat1$sample!="HCC_17",]) %>% as.data.frame(stringsAsFactors=FALSE)

readsta <- seldat[,c(1,2,4,5)] %>% melt(by="sample") %>% as.data.frame()
readsum <- summarySE(readsta, measurevar="value", groupvars=c("variable"))

p1 <- ggplot(readsta,aes(x=variable,y=value,fill = variable)) +
  geom_bar(stat = "identity", data = readsum,width=0.85) +
  geom_errorbar(
    aes(ymin = value-se, ymax = value+se),
    data = readsum, width = 0.2) + 
  geom_text(data=readsum, y= readsum$value*1.1, label = c("",round(mean(seldat$`%mapped`)*100,1), round(mean(seldat$`%rmdup_properly_mapped`)*100,1)))+
  scale_fill_manual(values=c("grey","#2d99ba","#a86445")) +
  mytheme + theme(legend.position = "None") +
  ylab("Number of reads")


p2 <- ggplot(seldat[seldat$lamba_conversion>0.9,c(1,8,9)] %>% melt(by="sample") %>% as.data.frame() ,aes(x=variable,y=value*100,fill = variable)) +
  geom_boxplot(outlier.shape = NA, color="#2d99ba", fill="white") +
  geom_jitter(position = position_jitter(0.2),size=0.1,color = "black", alpha=0.8) +
  mytheme + theme(legend.position = "None") +
  ylab("Fraction of converted \n cytosine (%)")

p3 <- ggplot(seldat[seldat$lamba_conversion>0.9,c(1,9)] %>% melt(by="sample") %>% as.data.frame() ,aes(x=variable,y=value*100,fill = variable)) +
  geom_boxplot(outlier.shape = NA, color="#2d99ba",fill="white") +
  geom_jitter(position = position_jitter(0.2),size=0.1,color = "black", alpha=0.8) +
  mytheme + ylim(0,0.35) + theme(legend.position = "None") +
  ylab("")


p <- plot_grid(p1, p2, p3, labels = c('a','b',''), label_size = 12,nrow=1, rel_widths = c(4, 2, 1))
ggsave("map_conversion_sta.pdf",p,width=8,height=3)

