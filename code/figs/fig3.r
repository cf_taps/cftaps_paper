library(readr)
library(tidyverse)
library(reshape2)
library(dplyr)
library(caret)
library(glmnet)
library(ROCR)
library(ggplot2)
library(RColorBrewer)
library(factoextra)
library(pryr)
library(cowplot)
library(ggplotify)
library(RColorBrewer)
library(MLmetrics)
library(GenomicRanges)
library(plotROC)
library(pheatmap)
library(pROC)
library(randomForest)
library(scales)
library(beeswarm)
source("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/code/functions_for_r.r")
source("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/figs/code/functions.r")

# Fig S4A (Tissue map) ----
setwd("/users/ludwig/cfo155/cfo155/cfDNA/cfDNA_clean/cftaps_paper/plots/")
filename <- "/gpfs2/well/ludwig/users/dyp502/revisions/tsne_transformed_tissue_df.csv"
dat <- read.table(filename, header=TRUE,stringsAsFactors=FALSE, sep=",")
p <- ggplot(dat,aes(x=TSNE1,y=TSNE2,color=tissue_group)) +
  geom_point(aes(shape = tissue_group)) +
  scale_shape_manual(values = c(1:11,1:11,1:11))+
  mytheme
ggsave(gsub("csv","pdf",basename(filename)),p,width=10,height=5)

filename <- "/gpfs2/well/ludwig/users/dyp502/revisions/pca_transformed_tissue_df.csv"
dat <- read.table(filename, header=TRUE,stringsAsFactors=FALSE, sep=",")
p <- ggplot(dat,aes(x=PCA1,y=PCA2,color=tissue_group)) +
  geom_point(aes(shape = tissue_group)) +
  scale_shape_manual(values = c(1:11,1:11,1:11))+
  mytheme + xlab("PC1") + ylab("PC2")
ggsave(gsub("csv","pdf",basename(filename)),p,width=10,height=5)

# Tissue contribution ----
filename <- "/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/2class/sample_mixtures_justEnhancers_MethAtlas_DMRs_bcells_tcells_neutrophils_dendriticells_separated_095_200_1000.csv"
dat <- read.table(filename, header=TRUE, stringsAsFactors=FALSE, sep="\t")
dat <- dat[,-grep("NHCCKAMI035",colnames(dat))]
dat.m <- melt(dat,id.vars=c("atlas_tissues"))
dat.m$atlas_tissues[dat.m$atlas_tissues %in% c("immature neutrophil","mature neutrophil")] <- "neutrophil"
dat.m$atlas_tissues[dat.m$atlas_tissues %in% c("immature conventional dendritic cell","mature conventional dendritic cell")] <- "dendritic cell"
dat.m <- aggregate(dat.m$value, by=list(Category=dat.m$variable, dat.m$atlas_tissues ), FUN = sum) %>% as.data.frame()
colnames(dat.m) <- c("variable","atlas_tissues","value")
dat.m$id <- str_split_fixed(dat.m$variable,"_",2)[,2]

smp <- read.table("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/resource/cfDNA_cohort_1_sample_list_11302020.excured.newid.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)
dat.m <- merge(dat.m, smp, by.x="id",by.y="Sample_ID")

tissue_group <- aggregate(dat.m$value, by=list(Category=dat.m$Status,dat.m$atlas_tissues ), FUN = mean) %>% as.data.frame()
colnames(tissue_group) <- c("status","tissue","value")

## Fig 3A & Fig S4B (Mean contribution in tissue) ----
for(i in c("HCC","PDAC","Ctrl")){
  seldat <- tissue_group[tissue_group$status==i,]
  seldat$tissue[seldat$value<=0.015] <- "Other"
  seldat <- aggregate(seldat$value, by=list(Category=seldat$status,seldat$tissue ), FUN = sum) %>% as.data.frame()
  colnames(seldat) <- c("status","tissue","value")
  seldat$value <- round(seldat$value,2) * 100
  seldat <- seldat[order(seldat$value),]
  seldat$tissue <- factor(seldat$tissue,levels=seldat$tissue)
  seldat <- seldat %>%
    arrange(desc(tissue)) %>%
    mutate(lab.ypos = cumsum(value) - 0.5*value)
  nb.cols <- nrow(seldat)
  mycolors <- rev(colorRampPalette(brewer.pal(8, "Set2"))(nb.cols))
  
  p  <- ggplot(data = seldat, aes(x = "", y = value, fill = tissue)) + 
    geom_bar(width = 1, stat = "identity", color = "white") +
    coord_polar("y", start = 0)+
    geom_text(aes(y = lab.ypos, label = value), color = "white",size=3) +
    scale_fill_manual(values = mycolors) +
    theme_void() +
    ggtitle(i)
  ggsave(gsub("csv",paste0(i,".pdf"),basename(filename)),p,width=5,height=5)
}

# Fig 3B & Fig S4C (Mean contribution in tissue) Liver and T cell contribution ----
beeswarm <- beeswarm(value ~ Status, data = dat.m[dat.m$atlas_tissues=="liver cancer" & dat.m$Status %in% c("Ctrl","HCC","PDAC"),],
                     method = 'swarm', spacing=2.8) [, c(1, 2, 6)]
colnames(beeswarm) <- c("x", "y", "status") 

p1 <- ggplot(beeswarm) +
  geom_boxplot(data=beeswarm, aes(status, y, fill=status), width=0.8, outlier.shape = NA) +
  scale_fill_manual(values=c("grey","#A73030FF","#0073C2")) + theme(legend.position = "none") +
  geom_point(aes(x,y), size=0.8) +
  mytheme + xlab("") +
  scale_y_continuous(limits = c(0, 0.17), breaks = seq(0, 0.20, by = 0.05))

ggsave(gsub("csv",".sel_tissue.boxplot.liver_cancer.ori.pdf",basename(filename)),p1,width=3,height = 6)

beeswarm <- beeswarm(value ~ Status, data = dat.m[dat.m$atlas_tissues=="memory_tcell" & dat.m$Status %in% c("Ctrl","HCC","PDAC"),],
                     method = 'swarm', spacing=0.5) [, c(1, 2, 6)]
colnames(beeswarm) <- c("x", "y", "status") 

p1 <- ggplot(beeswarm) +
  geom_boxplot(data=beeswarm, aes(status, y, fill=status), width=0.8, outlier.shape = NA) +
  scale_fill_manual(values=c("grey","#A73030FF","#0073C2")) + theme(legend.position = "none") +
  geom_point(aes(x,y), size=0.8) +
  mytheme + xlab("") 

ggsave(gsub("csv",".sel_tissue.boxplot.memory_tcell.ori.pdf",basename(filename)),p1,width=4,height = 6)

# Statistic test ----
setwd("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/3class/tiscon/sample_mixtures_oct")
for(filename in list.files(pattern="sample_mixtures_justEnhancers_MethAtlas_DMRs_bcells_tcells_neutrophils_dendriticells_separated_095_200_1000.csv$") ){
  cat("\n",filename,"\n")
  dat <- read.table(filename, header=TRUE, stringsAsFactors=FALSE, sep="\t")
  dat.m <- melt(dat,id.vars=c("atlas_tissues"))
  dat.m$status <- str_split_fixed(dat.m$variable,"_",2)[,1]
  seldat.m <- dat.m[dat.m$status=="Healthy"|dat.m$status=="PDAC"|dat.m$status=="HCC",]
  seldat.m$value <- seldat.m$value*100
  seldat.m$variable <- str_split_fixed(seldat.m$variable,"_",2)[,2]
  seldat.m$status <- factor(seldat.m$status,levels=c("Healthy","HCC","PDAC"))
  # cat("tissue","hcc.vs.healthy","pdac.vs.healthy","\n")
  for(i in unique(seldat.m$atlas_tissues)){
    cat(gsub(" ","_",i),
        wilcox.test(seldat.m[seldat.m$atlas_tissues==i&seldat.m$status=="HCC",3], seldat.m[seldat.m$atlas_tissues==i&seldat.m$status=="Healthy",3], paired=FALSE)$p.value,
        wilcox.test(seldat.m[seldat.m$atlas_tissues==i&seldat.m$status=="PDAC",3], seldat.m[seldat.m$atlas_tissues==i&seldat.m$status=="Healthy",3], paired=FALSE)$p.value,
        t.test(seldat.m[seldat.m$atlas_tissues==i&seldat.m$status=="HCC",3], seldat.m[seldat.m$atlas_tissues==i&seldat.m$status=="Healthy",3], paired=FALSE)$p.value,
        t.test(seldat.m[seldat.m$atlas_tissues==i&seldat.m$status=="PDAC",3], seldat.m[seldat.m$atlas_tissues==i&seldat.m$status=="Healthy",3], paired=FALSE)$p.value, "\n")
    
  }
}

# Fig S4D-I ----
## hcc 
source("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/figs/code/functions.r")
scorefile <- "/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/2class/excured/sample_mixtures_justEnhancers_MethAtlas_DMRs_bcells_tcells_neutrophils_dendriticells_separated_095_200_1000.csv.HCC_allHealthy.add_Cirrhosis.excured.txt"
smps <- read.table("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/resource/cfDNA_cohort_1_sample_list_11302020.excured.newid.txt",header = TRUE, sep="\t",stringsAsFactors = FALSE)
cancer <-"HCC"
addtest <- "Cirrhosis"

scores <- read.table(scorefile,header=TRUE)
scores <- merge(scores,smps[,c(1,10)],by.x="smp",by.y="Sample_ID")
colnames(scores)[11] <- "id"
scores$id <- gsub("trl_|CC_|DAC_","",scores$id)
fac <- with(scores, reorder(id,cvglm, median, order = TRUE))
scores$id <- factor(scores$id, levels = levels(fac))
scores$Status <- factor(scores$Status, levels=c("Ctrl",cancer, addtest))
scores$truth <- as.numeric(scores$Status==cancer)
mlauc1 <- round(AUC(scores[scores$Status!=addtest,]$cvglm, scores[scores$Status!=addtest,]$truth),2)
roc_c <- roc(truth ~ cvglm,  scores[scores$Status!=addtest,], percent=TRUE)
cutoff <- coords(roc_c, "best", ret=c("threshold","specificity"), transpose = FALSE)

p1 <- ggplot() +
  geom_roc(data=scores[scores$Status!=addtest,], aes(d = truth, m = cvglm), labels = FALSE, color="#A73030FF") +
  theme_classic() +
  annotate(geom="text", x=0.3, y=0.6, label=paste0(cancer," AUC: ",mlauc1),color="#A73030FF",size=5) +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"))
scores$label <- "loo"
scores$label[scores$Status==addtest] <- "add"

p2 <- ggplot(scores[scores$Status!=addtest,]) + 
  geom_point(aes(x=id, y=cvglm, color=Status)) +
  facet_grid(~label, scales = "free_y")+ylim(0,1)+
  scale_color_manual(values=c("grey","#A73030FF")) + 
  geom_hline(yintercept = cutoff$threshold, linetype =3) + mytheme
scores <- scores[order(scores$cvglm),]
rownames(scores) <- make.names(scores$smp, unique = TRUE, allow_ = TRUE)
fac <- with(scores, reorder(id, cvglm, median, order = TRUE))
scores$id <- factor(scores$id, levels = levels(fac))

scores$idx <- as.numeric(str_split_fixed(rownames(scores),"\\.",2)[,2])+1
scores$idx[is.na(scores$idx)] <-1
sumscores <- summarySE(scores[scores$Status==addtest,],measurevar = "cvglm",groupvars = "id")
sumscores$N <- sumscores$N/2
selscores <- merge(scores[scores$Status==addtest,],sumscores,by="id")
p3 <- ggplot(selscores) + 
  geom_point(aes(x=idx, y=cvglm.x, color=Status),size=0.8,alpha=0.8) + 
  geom_pointrange(aes(x=N, y=median, ymin=median-sd, ymax=median+sd),color="grey30") +
  facet_grid(~id, scales = "free_y")+
  scale_color_manual(values="#629bb9") + geom_hline(yintercept = cutoff$threshold, linetype =3) +
  mytheme + theme(legend.position = "none")

p <- plot_grid(p1,p2+theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)),p3, label_size = 12, nrow=1,rel_widths = c(0.7,1.5,0.6))
ggsave(gsub("txt",paste0(addtest,".pdf"),basename(scorefile)),p,width=12,height=3)
cat(cutoff$threshold,"\n")

## pdac

scorefile <- "/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/2class/excured/sample_mixtures_justEnhancers_MethAtlas_DMRs_bcells_tcells_neutrophils_dendriticells_separated_095_200_1000.csv.PDAC_allHealthy.add_Pancreatitis.excured.txt"
cancer <-"PDAC"
addtest <- "Pancreatitis"

scores <- read.table(scorefile,header=TRUE)
scores <- merge(scores,smps[,c(1,10)],by.x="smp",by.y="Sample_ID")
colnames(scores)[11] <- "id"
scores$id <- gsub("trl_|CC_|DAC_","",scores$id)
fac <- with(scores, reorder(id, cvglm, median, order = TRUE))
scores$id <- factor(scores$id, levels = levels(fac))
scores$Status <- factor(scores$Status, levels=c("Ctrl",cancer, addtest))
scores$truth <- as.numeric(scores$Status==cancer)
mlauc1 <- round(AUC(scores[scores$Status!=addtest,]$cvglm, scores[scores$Status!=addtest,]$truth),2)
p1 <- ggplot() +
  geom_roc(data=scores[scores$Status!=addtest,], aes(d = truth, m = cvglm), labels = FALSE, color="#0073C2FF") +
  theme_classic() +
  annotate(geom="text", x=0.3, y=0.6, label=paste0(cancer," AUC: ",mlauc1),color="#0073C2FF",size=5) +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"))
scores$label <- "loo"
scores$label[scores$Status==addtest] <- "add"
p2 <- ggplot(scores[scores$Status!=addtest,], aes(x=id, y=cvglm, color=Status)) + 
  geom_point() +
  facet_grid(~label, scales = "free_y")+ylim(0,1)+
  scale_color_manual(values=c("grey","#0073C2FF")) + 
  geom_hline(yintercept = cutoff$threshold, linetype =3) + mytheme
scores <- scores[order(scores$cvglm),]
rownames(scores) <- make.names(scores$smp, unique = TRUE, allow_ = TRUE)
fac <- with(scores, reorder(id, cvglm, median, order = TRUE))
scores$id <- factor(scores$id, levels = levels(fac))

scores$idx <- as.numeric(str_split_fixed(rownames(scores),"\\.",2)[,2])+1
scores$idx[is.na(scores$idx)] <-1
sumscores <- summarySE(scores[scores$Status==addtest,],measurevar = "cvglm",groupvars = "id")
sumscores$N <- sumscores$N/2
selscores <- merge(scores[scores$Status==addtest,],sumscores,by="id")
p3 <- ggplot(selscores) + 
  geom_point(aes(x=idx, y=cvglm.x, color=Status),size=0.8,alpha=0.8) + 
  geom_pointrange(aes(x=N, y=median, ymin=median-sd, ymax=median+sd),color="grey30") +
  facet_grid(~id, scales = "free_y") +
  scale_color_manual(values="#a08c41") + geom_hline(yintercept = cutoff$threshold, linetype =3) +
  mytheme + theme(legend.position = "none")+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))

p <- plot_grid(p1,p2+theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)),p3, label_size = 12, nrow=1,rel_widths = c(0.7,1.5,0.6))
ggsave(gsub("txt",paste0(addtest,".pdf"),basename(scorefile)),p,width=12,height=3)
cat(cutoff$threshold,"\n")

# Fragments ----

setwd("/users/ludwig/cfo155/cfo155/cfDNA/cfDNA_clean/cftaps_paper/plots/")
smps <- read.table("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/resource/cfDNA_cohort_1_sample_list_11302020.excured.newid.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)
dat2 <- read.table("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/frags/all.frag.length.sta.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)
colnames(dat2) <- gsub("Healthy_NHCCJORO037", "Cirrhosis_NHCCJORO037",colnames(dat2))
dat2 <- dat2[,-grep("NHCCKAMI035",colnames(dat2))]
seldat <- dat2[dat2$length<500 & dat2$length>70,]
seldat.l <- cbind("len"=seldat[,1],apply(seldat[,-c(1)],2,function(x){x/sum(x)}) %>% as.data.frame())

# Fig 3C (fragments distribution) ----
# http://www.cookbook-r.com/Graphs/Plotting_means_and_error_bars_(ggplot2)/
seldat.m <- melt(seldat.l,id.vars = c("len"))
seldat.m$group <- str_split_fixed(seldat.m$variable,"_",2)[,1]
seldat.m$smp <- str_split_fixed(seldat.m$variable,"_",2)[,2]
seldat.m <- seldat.m[seldat.m$smp%in%smps$Sample_ID,]
seldat.s <- summarySE(seldat.m, measurevar="value", groupvars=c("len", "group"))


seldat.s$CI_lower <- seldat.s$value - seldat.s$se
seldat.s$CI_upper <- seldat.s$value + seldat.s$se
seldat.s$group <- factor(seldat.s$group, levels=c("PDAC","HCC","Healthy","Cirrhosis","Pancreatitis"))
p <- ggplot(seldat.s[seldat.s$group%in%c("Healthy","PDAC","HCC"),], aes(x=len, y=value,color=group)) +
  geom_line(size=1, alpha=0.8) +
  geom_ribbon(aes(ymin=CI_lower, ymax=CI_upper,fill=group), alpha=0.1) +
  scale_fill_manual(values=c("#0073C2FF","#A73030FF","grey")) +
  scale_color_manual(values=c("#0073C2FF","#A73030FF","grey"))+
  # mytheme +
  theme(legend.position = "bottom") +
  scale_x_continuous(breaks = seq(70,500,20), 
                     limits = c(70, 500),
                     labels = seq(70,500,20)) +
  theme(panel.grid.minor.x =  element_line(colour="grey",size = rel(0.5),linetype = 3),
        panel.grid.major.x =  element_line(colour="grey",size = rel(0.5),linetype = 3),
        panel.grid.minor.y =  element_blank(),
        panel.grid.major.y =  element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"))
p
ggsave("frag_len.cftaps.pdf",p,width=5.5,height=3)

# Fig 3D (boxplot) ----
seldat <- dat2[dat2$length<500 & dat2$length>70,]
seldat$length <- cut(seldat$length,breaks=c(70,150,300,500),include.lowest = TRUE)
seldat.l <- aggregate(. ~ length, seldat, sum)
seldat.s <- apply(seldat.l[,-c(1)],2,function(x){x/sum(x)}) %>% as.data.frame()
rownames(seldat.s) <- seldat.l[,1]
seldat.s <- t(seldat.s) %>% as.data.frame(stringsAsFactors=FALSE)
seldat.s$status <- str_split_fixed(rownames(seldat.s),"_",2)[,1]
seldat.m <- melt(seldat.s,id.vars = c("status"))
seldat.m$status <- factor(seldat.m$status, levels=c("Healthy","HCC","Cirrhosis","PDAC","Pancreatitis","PDACpeak"))
p <- ggplot(seldat.m[(seldat.m$status %in% c("HCC","Healthy","PDAC")) & (seldat.m$variable %in% c("[70,150]","(300,500]")),],aes(x=status,y=value)) + 
  geom_boxplot(outlier.shape = NA) + facet_wrap(~variable , nrow=1) +
  geom_jitter(aes(color = status,alpha=0.5),position=position_jitterdodge(0.4, seed=2, dodge.width = 0.8)) +
  scale_color_manual(values=c("grey","#A73030FF","#0073C2FF")) +
  mytheme + 
  theme(legend.position = "bottom")
ggsave("frag_len.cftaps.boxplot.pdf",p,width=3.5,height=4)   
t.test(seldat.m[seldat.m$status=="HCC" & seldat.m$variable=="[70,150]",3],seldat.m[seldat.m$status=="Healthy" & seldat.m$variable=="[70,150]",3])
t.test(seldat.m[seldat.m$status=="PDAC" & seldat.m$variable=="[70,150]",3],seldat.m[seldat.m$status=="Healthy" & seldat.m$variable=="[70,150]",3])
t.test(seldat.m[seldat.m$status=="HCC" & seldat.m$variable=="(300,500]",3],seldat.m[seldat.m$status=="Healthy" & seldat.m$variable=="(300,500]",3])
t.test(seldat.m[seldat.m$status=="PDAC" & seldat.m$variable=="(300,500]",3],seldat.m[seldat.m$status=="Healthy" & seldat.m$variable=="(300,500]",3])
wilcox.test(seldat.m[seldat.m$status=="HCC" & seldat.m$variable=="[70,150]",3],seldat.m[seldat.m$status=="Healthy" & seldat.m$variable=="[70,150]",3])
wilcox.test(seldat.m[seldat.m$status=="PDAC" & seldat.m$variable=="[70,150]",3],seldat.m[seldat.m$status=="Healthy" & seldat.m$variable=="[70,150]",3])
wilcox.test(seldat.m[seldat.m$status=="HCC" & seldat.m$variable=="(300,500]",3],seldat.m[seldat.m$status=="Healthy" & seldat.m$variable=="(300,500]",3])
wilcox.test(seldat.m[seldat.m$status=="PDAC" & seldat.m$variable=="(300,500]",3],seldat.m[seldat.m$status=="Healthy" & seldat.m$variable=="(300,500]",3])

# Fig 3E (PCA) ----
dat2 <- read.table("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/frags/all.frag.length.sta.long.310-500.ratio.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)
colnames(dat2) <- gsub("Healthy_NHCCJORO037", "Cirrhosis_NHCCJORO037",colnames(dat2))
dat2 <- dat2[,-grep("NHCCKAMI035",colnames(dat2))]

plot_pca <- function(dat, scale=FALSE, sel_pc, smp_status, outputlabel, selcolor ){
  res_pca <- prcomp(t(dat), scale = scale)
  p1 <- fviz_pca_ind(
    X = res_pca,
    axes = c(sel_pc),
    habillage = smp_status,
    #addEllipses = TRUE,
    mean.point = FALSE,
    repel = TRUE,
    label = "all",
    labelsize = 2
  ) +labs(title = outputlabel) +
    scale_color_manual(values=selcolor)
  p2 <- fviz_pca_ind(
    X = res_pca,
    axes = c(sel_pc),
    habillage = smp_status,
    #addEllipses = TRUE,
    mean.point = FALSE,
    repel = TRUE,
    label = "none",
    pointsize = 3
  )+labs(title = outputlabel) + 
    scale_color_manual(values=selcolor) +
    theme(panel.grid.major = element_blank(), 
          panel.grid.minor = element_blank(),
          panel.background = element_blank())
  p3 <- fviz_eig(res_pca, addlabels = TRUE)
  ggsave(paste0(outputlabel, ".label.pdf"),p1,width=5,height=5)
  ggsave(paste0(outputlabel, ".nlabel.pdf"),p2,width=5,height=4.3)
  ggsave(paste0(outputlabel, ".eig.pdf"),p3,width=5,height=5)
}

ratios <- dat2[,-1]
colnames(ratios) <- gsub("Healthy","Ctrl",colnames(ratios))
selid <- "Ctrl_|PDAC_"
selsmp <- ratios[,grep(selid,colnames(ratios))]
smpstatus <- unlist(lapply(colnames(selsmp),function(x)str_split(x,pattern="_")[[1]][1]))
outfix <- paste0("all.frag.length.sta.long.310-500.",gsub("\\||_$","",selid))
plot_pca(selsmp, scale=FALSE, sel_pc <- c(1,2), smp_status=smpstatus, outputlabel= paste0(outfix,".pca"),selcolor=c("grey","#0073C2FF"))
selid <- "Ctrl_|HCC_"
selsmp <- ratios[,grep(selid,colnames(ratios))]
smpstatus <- unlist(lapply(colnames(selsmp),function(x)str_split(x,pattern="_")[[1]][1]))
outfix <- paste0("all.frag.length.sta.long.310-500.",gsub("\\||_$","",selid))
plot_pca(selsmp, scale=FALSE, sel_pc <- c(1,2), smp_status=smpstatus, outputlabel= paste0(outfix,".pca"),selcolor=c("grey","#A73030FF"))



# Fig S6A (WGBS) ----
dat1 <- read.table("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/cancerdetector/cancerdetector_wgbs/processed/bsMapping/all.frag.length.sta.txt",header=TRUE,stringsAsFactors=FALSE)
colnames(dat1) <- c("length",paste0("tissue_",colnames(dat1)[2:3]), paste0("HCC_",colnames(dat1)[4:7]),paste0("Healthywgbs_",colnames(dat1)[8:11]))
seldat <- dat1[dat1$length<500 & dat1$length>70,]
seldat.l <- cbind("len"=seldat[,1],apply(seldat[,-c(1)],2,function(x){x/sum(x)}) %>% as.data.frame())
seldat.m <- melt(seldat.l,id.vars = c("len"))
seldat.m$group <- str_split_fixed(seldat.m$variable,"_",2)[,1]
seldat.m$smp <- str_split_fixed(seldat.m$variable,"_",2)[,2]

seldat.s <- summarySE(seldat.m, measurevar="value", groupvars=c("len", "group"))

seldat.s$CI_lower <- seldat.s$value - seldat.s$se
seldat.s$CI_upper <- seldat.s$value + seldat.s$se

p <- ggplot(seldat.s[seldat.s$group%in%c("Healthywgbs"),], aes(x=len, y=value,color=group)) +
  geom_line(size=1, alpha=0.8) +
  geom_ribbon(aes(ymin=CI_lower, ymax=CI_upper,fill=group), alpha=0.1) +
  scale_fill_manual(values=c("grey")) +
  scale_color_manual(values=c("grey"))+
  # mytheme +
  theme(legend.position = "bottom") +
  scale_x_continuous(breaks = seq(70,500,20), 
                     limits = c(70, 500),
                     labels = seq(70,500,20)) +
  theme(panel.grid.minor.x =  element_line(colour="grey",size = rel(0.5),linetype = 3),
        panel.grid.major.x =  element_line(colour="grey",size = rel(0.5),linetype = 3),
        panel.grid.minor.y =  element_blank(),
        panel.grid.major.y =  element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"))
p
ggsave("frag_len.cfwgbs.pdf",p,width=6,height=3)


# Fig S6B-G ----
## hcc
scorefile <- "/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/2class/excured/all.frag.length.sta.long.310-500.ratio.txt.HCC_allHealthy.add_Cirrhosis.excured.txt"
cancer <-"HCC"
addtest <- "Cirrhosis"

scores <- read.table(scorefile,header=TRUE)
scores <- merge(scores,smps[,c(1,10)],by.x="smp",by.y="Sample_ID")
colnames(scores)[11] <- "id"
scores$id <- gsub("trl_|CC_|DAC_","",scores$id)
fac <- with(scores, reorder(id,cvglm, median, order = TRUE))
scores$id <- factor(scores$id, levels = levels(fac))
scores$Status <- factor(scores$Status, levels=c("Ctrl",cancer, addtest))
scores$truth <- as.numeric(scores$Status==cancer)
mlauc1 <- round(AUC(scores[scores$Status!=addtest,]$cvglm, scores[scores$Status!=addtest,]$truth),2)
roc_c <- roc(truth ~ cvglm,  scores[scores$Status!=addtest,], percent=TRUE)
cutoff <- coords(roc_c, "best", ret=c("threshold","specificity"), transpose = FALSE)

p1 <- ggplot() +
  geom_roc(data=scores[scores$Status!=addtest,], aes(d = truth, m = cvglm), labels = FALSE, color="#A73030FF") +
  theme_classic() +
  annotate(geom="text", x=0.3, y=0.6, label=paste0(cancer," AUC: ",mlauc1),color="#A73030FF",size=5) +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"))
scores$label <- "loo"
scores$label[scores$Status==addtest] <- "add"

p2 <- ggplot(scores[scores$Status!=addtest,]) + 
  geom_point(aes(x=id, y=cvglm, color=Status)) +
  facet_grid(~label, scales = "free_y")+ylim(0,1)+
  scale_color_manual(values=c("grey","#A73030FF")) + 
  geom_hline(yintercept = cutoff$threshold, linetype =3) + mytheme
scores <- scores[order(scores$cvglm),]
rownames(scores) <- make.names(scores$smp, unique = TRUE, allow_ = TRUE)
fac <- with(scores, reorder(id, cvglm, median, order = TRUE))
scores$id <- factor(scores$id, levels = levels(fac))

scores$idx <- as.numeric(str_split_fixed(rownames(scores),"\\.",2)[,2])+1
scores$idx[is.na(scores$idx)] <-1
sumscores <- summarySE(scores[scores$Status==addtest,],measurevar = "cvglm",groupvars = "id")
sumscores$N <- sumscores$N/2
selscores <- merge(scores[scores$Status==addtest,],sumscores,by="id")
p3 <- ggplot(selscores) + 
  geom_point(aes(x=idx, y=cvglm.x, color=Status),size=0.8,alpha=0.8) + 
  geom_pointrange(aes(x=N, y=median, ymin=median-sd, ymax=median+sd),color="grey30") +
  facet_grid(~id, scales = "free_y")+ ylim(0,1.02)+
  scale_color_manual(values="#629bb9") + geom_hline(yintercept = cutoff$threshold, linetype =3) +
  mytheme + theme(legend.position = "none")

p <- plot_grid(p1,p2+theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)),p3, label_size = 12, nrow=1,rel_widths = c(0.7,1.5,0.6))
ggsave(gsub("txt",paste0(addtest,".pdf"),basename(scorefile)),p,width=12,height=3)
cat(cutoff$threshold,"\n")

## pdac
scorefile <- "/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/2class/excured/all.frag.length.sta.long.310-500.ratio.txt.PDAC_allHealthy.add_Pancreatitis.excured.txt"
cancer <-"PDAC"
addtest <- "Pancreatitis"

scores <- read.table(scorefile,header=TRUE)
scores <- merge(scores,smps[,c(1,10)],by.x="smp",by.y="Sample_ID")
colnames(scores)[11] <- "id"
scores$id <- gsub("trl_|CC_|DAC_","",scores$id)
fac <- with(scores, reorder(id, cvglm, median, order = TRUE))
scores$id <- factor(scores$id, levels = levels(fac))
scores$Status <- factor(scores$Status, levels=c("Ctrl",cancer, addtest))
scores$truth <- as.numeric(scores$Status==cancer)
mlauc1 <- round(AUC(scores[scores$Status!=addtest,]$cvglm, scores[scores$Status!=addtest,]$truth),2)
roc_c <- roc(truth ~ cvglm,  scores[scores$Status!=addtest,], percent=TRUE)
cutoff <- coords(roc_c, "best", ret=c("threshold","specificity"), transpose = FALSE)

p1 <- ggplot() +
  geom_roc(data=scores[scores$Status!=addtest,], aes(d = truth, m = cvglm), labels = FALSE, color="#0073C2FF") +
  theme_classic() +
  annotate(geom="text", x=0.3, y=0.6, label=paste0(cancer," AUC: ",mlauc1),color="#0073C2FF",size=5) +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(), axis.line = element_line(colour = "black"))
scores$label <- "loo"
scores$label[scores$Status==addtest] <- "add"
p2 <- ggplot(scores[scores$Status!=addtest,], aes(x=id, y=cvglm, color=Status)) + 
  geom_point() +
  facet_grid(~label, scales = "free_y")+ylim(0,1)+
  scale_color_manual(values=c("grey","#0073C2FF")) + 
  geom_hline(yintercept = cutoff$threshold, linetype =3) + mytheme
scores <- scores[order(scores$cvglm),]
rownames(scores) <- make.names(scores$smp, unique = TRUE, allow_ = TRUE)
fac <- with(scores, reorder(id, cvglm, median, order = TRUE))
scores$id <- factor(scores$id, levels = levels(fac))

scores$idx <- as.numeric(str_split_fixed(rownames(scores),"\\.",2)[,2])+1
scores$idx[is.na(scores$idx)] <-1
sumscores <- summarySE(scores[scores$Status==addtest,],measurevar = "cvglm",groupvars = "id")
sumscores$N <- sumscores$N/2
selscores <- merge(scores[scores$Status==addtest,],sumscores,by="id")
p3 <- ggplot(selscores) + 
  geom_point(aes(x=idx, y=cvglm.x, color=Status),size=0.8,alpha=0.8) + 
  geom_pointrange(aes(x=N, y=median, ymin=median-sd, ymax=median+sd),color="grey30") +
  facet_grid(~id, scales = "free_y")+ ylim(0,1.02)+
  scale_color_manual(values="#a08c41") + geom_hline(yintercept = cutoff$threshold, linetype =3) +
  mytheme + theme(legend.position = "none")+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))

p <- plot_grid(p1,p2+theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)),p3, label_size = 12, nrow=1,rel_widths = c(0.7,1.5,0.6))
ggsave(gsub("txt",paste0(addtest,".pdf"),basename(scorefile)),p,width=12,height=3)
cat(cutoff$threshold,"\n")










