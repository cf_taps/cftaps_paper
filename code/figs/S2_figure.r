library(ggplot2)
library(dplyr)
library(ggforce)
library(tidyr)
library(matrixStats)
library(grid)
library(gridExtra)
library(pastecs)
library(ggpubr)
library(data.table)
library(factoextra)
library(FactoMineR)
library(RColorBrewer)
library(plyr)
library(rowr)
library(reshape2)
library(varhandle)
library(smoother)



metadata<-read.csv("/mnt/sdb1/Paulina_DPhil/cfDNA_merged/resource/smp_sheet_checked.txt", sep="\t", stringsAsFactors = FALSE)
metadata$Sample_ID[which(metadata$Sample_ID=="TP-277")]<-"TP277"
metadata$Age_Range[which(metadata$Age=="18")]<-"10-19"
metadata$Status2<-metadata$Status
metadata$Status2[which(metadata$Sample_ID=="PT34")]<-"PDACpeak"
metadata$Status2[which(metadata$Sample_ID=="PT38")]<-"PDACpeak"
metadata$Status2[which(metadata$Sample_ID=="PT39")]<-"PDACpeak"
metadata$Status2[which(metadata$Sample_ID=="PT40")]<-"PDACpeak"
metadata$Status2[which(metadata$Sample_ID=="PT42")]<-"PDACpeak"
metadata$Status2[which(metadata$Sample_ID=="PT43")]<-"PDACpeak"
metadata$Status2[which(metadata$Sample_ID=="PT44")]<-"PDACpeak"
metadata$new.name<-paste(metadata$Status2, metadata$Sample_ID,sep="_")

metadata<-metadata%>%dplyr::filter(Status2!="PDACpeak")%>%
  dplyr::filter(Sample_ID!="GI4286"&Sample_ID!="NHCCKAMI035")
metadata2<-metadata%>%dplyr::filter(Status2!="PDACpeak")%>%
  dplyr::filter(Sample_ID!="PT52"&Sample_ID!="GI4284")

sup3<-read.table("/mnt/sdb1/Paulina_DPhil/paper/comorb.txt",
                 header=TRUE)
tmp<-merge(sup3,metadata2,by.y="Sample_ID",by.x="Sample_ID", all.x=TRUE)

#Figure S2A ----

p<-ggplot(data=metadata2, aes(x=reorder(Status,Age), y=Age))+
  geom_boxplot(outlier.shape = NA)+
  geom_jitter(position=position_jitter(0.1), aes(colour=Sex))+
  theme(axis.text.x = element_text(angle = 90))+
  theme_bw()+
  scale_color_manual(values=c("darkmagenta","darkgoldenrod2"))
p

#Figure S2B ----
#hist
df<-read.table("/mnt/sdb1/Paulina_DPhil/paper/cpg_meth_distribution.depth3.txt")
colnames(df)<-c("name","mod","count")
df$bin<-NA
df$bin<-ifelse(df$mod>=0 & df$mod<=0.1,1,
               ifelse(df$mod>0.1 & df$mod<=0.2,2,
                      ifelse(df$mod>0.2 & df$mod<=0.3,3,
                             ifelse(df$mod>0.3 & df$mod<=0.4,4,
                                    ifelse(df$mod>0.4 & df$mod<=0.5,5,
                                           ifelse(df$mod>0.5 & df$mod<=0.6,6,
                                                  ifelse(df$mod>0.6 & df$mod<=0.7,7,
                                                         ifelse(df$mod>0.7 & df$mod<=0.8,8,
                                                                ifelse(df$mod>0.8 & df$mod<=0.9,9,
                                                                       ifelse(df$mod>0.9 & df$mod<=1,10,NA))))))))))

df$status<-NA
df$status[grep("Cirrhosis_",df$name)]<-"Cirrhosis"
df$status[grep("HCC_",df$name)]<-"HCC"
df$status[grep("PDAC_",df$name)]<-"PDAC"
df$status[grep("Healthy_",df$name)]<-"Healthy"
df$status[grep("Pancreatitis_",df$name)]<-"Pancreatitis"

df<-df[-grep("Cirrhosis|PDACpeak|Pancreatitis", df$name),]
df$name<-as.character(df$name)
df<-aggregate(count ~ bin + name, FUN=sum, data=df)
sums<-aggregate(df$count, by=list(df$name), FUN=sum)
colnames(sums)<-c("name","total")
df <- merge(x = df, y = sums, by = "name")
df$freq<-(df$count/df$total)*100


df$status<-NA
df$status[grep("HCC_",df4$name)]<-"HCC"
df$status[grep("PDAC_",df4$name)]<-"PDAC"
df$status[grep("Healthy_",df4$name)]<-"Healthy"

df2<-aggregate(freq ~ bin + status, FUN=mean, data=df)
df2$status<-factor(df2$status, levels=c("Healthy","HCC","PDAC"), ordered=TRUE) 

df$mean_freq<-NA
df3 <- df %>% right_join(df2, by=c("bin","status"))
df3$status<-factor(df3$status, levels=c("Healthy","HCC","PDAC"), ordered=TRUE)

b.mod<-ggplot(df3)+
  geom_bar(aes(x=bin, y=freq.y),position="identity",stat="identity", color="black",fill="lightgrey")+
  geom_line(aes(x=bin, y=freq.x, colour=name),position="identity",stat="identity")+
  theme_classic()+
  facet_wrap(~ status)+
  theme(legend.position = "none")+
  ggtitle("meth") +
  ylab("% of modified CpG sites") +
  xlab("bin")
b.mod

#Figure S2C-F ----
df<-read.table("/mnt/sdb1/Paulina_DPhil/paper/chr_meth.txt",
               header=TRUE,
               stringsAsFactors = FALSE)
df$chr<-gsub(".sort.md.clip.10.140.75.75_CpG.bedGraph.gz","",df$chr)
tmp<-do.call(rbind,strsplit(df$chr,"_"))
colnames(tmp)<-c("status","name")
df<-data.frame(df,tmp)
df<-df[grep("HCC|PDAC|Healthy",df$status),]
df<-df[-grep("NHCCJORO037|PT52|NHCCKAMI035",df$name),]
df<-df[-grep("PDACpeak",df$status),]

size<-read.csv("/mnt/sdb1/Paulina_DPhil/paper/tumour_size.txt",sep="\t")
colnames(size)<-c("name","tumour_mm")
df2<-merge(df,size,by.x="name",all.x=TRUE)
df2$status <- factor(df2$status,levels = c("Healthy", "HCC", "PDAC"))
df2$stage2<-NA
df2$stage2[which(df2$Stage==NA)]<-"0"
df2$stage2[which(df2$Stage=="I")]<-"1"
df2$stage2[which(df2$Stage=="II")]<-"2"
df2$stage2[which(df2$Stage=="III")]<-"3"
df2$stage2[which(df2$Stage=="IV")]<-"4"
df2$stage2<-as.numeric(df2$stage2)
df2$name<-as.character(df2$name)

#S2C 
cor.test(df2$average_rC[grep("HCC",df2$status)],df2$tumour_mm[grep("HCC",df2$status)])
#p-value = 0.002756   
#cor 
# -0.619299

x<-df2[grep("HCC",df2$status),c(10,6)]
p1 <- ggplot(x, aes(x=tumour_mm, y=average_rC)) + 
  geom_point( color="#A93130") +
  geom_smooth(method=lm , color="black", linetype="dashed",size=0.5,fill="#A93130", se=TRUE) +
  theme_classic()+
  ylab("Mean genome modification (%)")+
  xlab("Tumour size (mm)")
p1

#S2D 
cor.test(df2$average_rC[grep("HCC",df2$status)],df2$stage2[grep("HCC",df2$status)])
#p-value = 0.03859
#cor
#-0.4542305 

x<-df2[grep("HCC",df2$status),c(11,6)]
p1 <- ggplot(x, aes(x=stage2, y=average_rC)) + 
  geom_point( color="#A93130") +
  geom_smooth(method=lm , color="black", linetype="dashed",size=0.5,fill="#A93130", se=TRUE) +
  theme_classic()+
  ylab("Mean genome modification (%)")+
  xlab("Stage")
p1

#S2E
cor.test(df2$average_rC[grep("PDAC",df2$status)],df2$tumour_mm[grep("PDAC",df2$status)])
#p-value = 0.6862
#cor
#[1] -0.08903254

x<-df2[grep("PDAC",df2$status),c(10,6)]
p1 <- ggplot(x, aes(x=tumour_mm, y=average_rC)) + 
  geom_point( color="#0d98ba") +
  geom_smooth(method=lm , color="black", linetype="dashed",size=0.5,fill="#0d98ba", se=TRUE) +
  theme_classic()+
  ylab("Mean genome modification (%)")+
  xlab("Tumour size (mm)")
p1

#S2F
cor.test(df2$average_rC[grep("PDAC",df2$status)],df2$stage2[grep("PDAC",df2$status)])
#p-value = 0.07095
#cor
#[1] -0.3833744

x<-df2[grep("PDAC",df2$status),c(11,6)]
p1 <- ggplot(x, aes(x=stage2, y=average_rC)) + 
  geom_point( color="#0d98ba") +
  geom_smooth(method=lm , color="black", linetype="dashed",size=0.5,fill="#0d98ba", se=TRUE) +
  theme_classic()+
  ylab("Mean genome modification (%)")+
  xlab("Stage")
p1

#Figure S3G ----
metadata<-read.csv("/mnt/sdb1/Paulina_DPhil/cfDNA_merged/resource/smp_sheet_checked.txt", sep="\t", stringsAsFactors = FALSE)
metadata$Sample_ID[which(metadata$Sample_ID=="TP-277")]<-"TP277"
metadata$Age_Range[which(metadata$Age=="18")]<-"10-19"
metadata$Status2<-metadata$Status
metadata$Status2[which(metadata$Sample_ID=="PT34")]<-"PDACpeak"
metadata$Status2[which(metadata$Sample_ID=="PT38")]<-"PDACpeak"
metadata$Status2[which(metadata$Sample_ID=="PT39")]<-"PDACpeak"
metadata$Status2[which(metadata$Sample_ID=="PT40")]<-"PDACpeak"
metadata$Status2[which(metadata$Sample_ID=="PT42")]<-"PDACpeak"
metadata$Status2[which(metadata$Sample_ID=="PT43")]<-"PDACpeak"
metadata$Status2[which(metadata$Sample_ID=="PT44")]<-"PDACpeak"

metadata$new.name<-paste(metadata$Status2, metadata$Sample_ID,sep="_")

df<-read.table("/mnt/sdb1/Paulina_DPhil/paper/chr_meth.txt",
               header=TRUE,
               stringsAsFactors = FALSE)
df$chr<-gsub(".sort.md.clip.10.140.75.75_CpG.bedGraph.gz","",df$chr)
tmp<-do.call(rbind,strsplit(df$chr,"_"))
colnames(tmp)<-c("status","name")
df<-data.frame(df,tmp)
df<-df[grep("HCC|PDAC|Healthy",df$status),]
df<-df[-grep("NHCCJORO037|PT52|NHCCKAMI035",df$name),]
df<-df[-grep("PDACpeak",df$status),]

#Figure S2G ----
#Methylation distribution hisogram chr4

#methylation in 1kb windows
#distribution

mat<-read.csv("/mnt/sdb1/Paulina_DPhil/cfDNA_merged/final_result/mat.ws1000.bulk.bed", 
              sep="\t", 
              stringsAsFactors = FALSE)
mat2<-mat

mat<-mat2%>%dplyr::filter(Chr=="chr4")

#prepare 1 mb windows
ws<-1000
mat$bin<-c(1:nrow(mat))
#index widows
for (i in grep ("bin", colnames(mat))){
  mat$index<-ceiling(mat[,i]/ws) 
}

mat[mat=="."]<-0
mat<-sapply(mat[,-1], as.numeric)
mat<-data.frame(mat)
mat.100kb<-aggregate(mat[, grep("mC|uC|nC", colnames(mat))],
                     by = list(mat$index),
                     FUN = sum)

for (indexmC in grep("_mC", colnames(mat.100kb))) { #index columns containing "_mC" in colname
  indexuC <- grep(gsub("_mC", "_uC", colnames(mat.100kb)[indexmC]), colnames(mat.100kb)) #index columns with uC
  m<-mat.100kb[,indexmC]/(mat.100kb[,indexuC]+mat.100kb[,indexmC])
  n<-ncol(mat.100kb)
  mat.100kb<-data.frame(mat.100kb,m)
  colnames(mat.100kb)[n+1]<-gsub("_mC", "_mod", colnames(mat.100kb)[indexmC]) 
}  

mod_df=mat.100kb[,grep("_mod",colnames(mat.100kb))]
mod_df

#smooth
smooth<-data.frame(mat.100kb$Group.1)

for (i in 1:ncol(mod_df)){
  name<-colnames(mod_df)[i]
  tmp<-mod_df[,i]
  tmp.smooth<-smth.gaussian(tmp,
                            window = 10,
                            tails=TRUE,
                            alpha=1)
  n<-ncol(smooth)
  smooth<-data.frame(smooth,tmp.smooth)
  colnames(smooth)[n+1]<-name
}

colnames(smooth)[which(colnames(smooth)=="Healthy_NHCCJORO037_mod")]<-"Cirrhosis_NHCCJORO037_mod"

healthy<-smooth[,grep("Healthy", colnames(smooth))]
healthy$bin<-c(1:nrow(healthy))
PDAC<-smooth[,grep("PDAC", colnames(smooth))]
PDAC<-PDAC[,-c(grep("PT43|PT44|PT38|PT42|PT40|PT39|PT34|PT52", colnames(PDAC)))]
PDAC$bin<-c(1:nrow(PDAC))
HCC<-smooth[,grep("HCC_", colnames(smooth))]
HCC$bin<-c(1:nrow(HCC))

healthy.melt<-melt(healthy, id="bin")
pdac.melt<-melt(PDAC,id="bin")
hcc.melt<-melt(HCC,id="bin")
hcc.melt<-hcc.melt[-c(grep("HCC_NHCCKAMI035_mod",hcc.melt$variable)),]

colours=colorspace::sequential_hcl(30)

p1<-ggplot(healthy.melt, aes(x=bin, y=value, colour=variable))+
  geom_line()+
  theme_bw()+
  theme(legend.position = "none")+
  ggtitle("Healthy") +
  xlab("Bin_1 mb") +
  ylab("Methylation value")+
  #scale_color_manual(values=pal)+
  ylim(0.60,0.85)
p1


p2<-ggplot(hcc.melt, aes(x=bin, y=value, colour=variable))+
  geom_line()+
  theme_bw()+
  theme(legend.position = "none")+
  ggtitle("HCC") +
  xlab("Bin_1 mb") +
  ylab("Methylation value")+
  #scale_color_manual(values=pal)+
  ylim(0.60,0.85)
p2

p3<-ggplot(pdac.melt, aes(x=bin, y=value, colour=variable))+
  geom_line()+
  theme_bw()+
  theme(legend.position = "none")+
  ggtitle("PDAC") +
  xlab("Bin_1 mb") +
  ylab("Methylation value")+
  #scale_color_manual(values=pal)+
  ylim(0.60,0.85)
p3

setwd("/mnt/sdb1/Paulina_DPhil/cfDNA_merged/figures_for_thesis/")

ggsave(p1,
       filename="healthy.chr4.meth.dist.pdf",
       width=8,
       height = 4)

ggsave(p2,
       filename="hcc.chr4.meth.dist.pdf",
       width=8,
       height = 4)

ggsave(p3,
       filename="pdac.chr4.meth.dist.pdf",
       width=8,
       height = 4)

#Figure S2H ----
#methylation variance
mat<-read.csv("/mnt/sdb1/Paulina_DPhil/cfDNA_merged/final_result/mat.ws1000.bulk.bed", 
              sep="\t", 
              stringsAsFactors = FALSE)
mat2<-mat
colnames(mat2)
colnames(mat2)[which(colnames(mat2)=="Healthy_NHCCJORO037_mC")]<-"Cirrhosis_NHCCJORO037_mC"
colnames(mat2)[which(colnames(mat2)=="Healthy_NHCCJORO037_uC")]<-"Cirrhosis_NHCCJORO037_uC"
colnames(mat2)[which(colnames(mat2)=="Healthy_NHCCJORO037_nC")]<-"Cirrhosis_NHCCJORO037_nC"

meth<-mat2
meth$idx <- floor(meth$Start / 1000000)  # generate index for each 1,000,000 bp bin
meth[meth=="."]<-0
meth<-sapply(meth[,-1], as.numeric)
meth<-data.frame(meth)
meth$Chr<-mat2$Chr

counts <- aggregate(meth[, grep("mC|nC|uC", colnames(meth))], by = list(meth$Chr, meth$idx), FUN=sum)  

#calculate methylation
for (indexmC in grep("_mC", colnames(counts))) { #index columns containing "_mC" in colname
  indexuC <- grep(gsub("_mC", "_uC", colnames(counts)[indexmC]), colnames(counts)) #index columns with uC
  m<-counts[,indexmC]/(counts[,indexuC]+counts[,indexmC])
  n<-ncol(counts)
  counts<-data.frame(counts,m)
  colnames(counts)[n+1]<-gsub("_mC", "_mod", colnames(counts)[indexmC]) 
}  

autosomes<-c("chr1$","chr2$","chr3$","chr4$","chr5$","chr6$","chr7$","chr8$","chr9$","chr10$",
             "chr11$","chr12$","chr13$","chr14$","chr15$","chr16$","chr17$","chr18$","chr19$","chr20$","chr21$","chr22$")

#select autosomes
counts<-counts[grep(paste(autosomes, collapse = "|"),counts$Group.1),]


mod_df=counts[,grep("_mod",colnames(counts))]
mod_df$pos<-paste(counts$Group.1, counts$Group.2, sep="_")
mod_df<-mod_df[complete.cases(mod_df),]
colnames(mod_df)<-gsub("_mod","",colnames(mod_df))

healthy<-mod_df[,grep("Healthy", colnames(mod_df))]
rownames(healthy)<-mod_df$pos
PDAC<-mod_df[,grep("PDAC", colnames(mod_df))]
PDAC<-PDAC[,-c(grep("PT43|PT44|PT38|PT42|PT40|PT39|PT34|PT52", colnames(PDAC)))]
rownames(PDAC)<-mod_df$pos
HCC<-mod_df[,grep("HCC_", colnames(mod_df))]
HCC<-HCC[,-c(grep("035", colnames(HCC)))]
rownames(HCC)<-mod_df$pos

healthy.var<-rowVars(as.matrix(healthy))
hcc.var<-rowVars(as.matrix(HCC))
pdac.var<-rowVars(as.matrix(PDAC))

var.df<-data.frame(mod_df$pos,healthy.var,hcc.var,pdac.var)
var.df<-var.df%>%tidyr::separate(mod_df.pos,c("chr","pos"),"_")
var.melt<-melt(var.df,id.vars = c("chr","pos"))

chrOrder <-c("chr1","chr2","chr3","chr4","chr5","chr6","chr7","chr8","chr9","chr10","chr11",
             "chr12","chr13","chr14","chr15","chr16","chr17","chr18","chr19","chr20",
             "chr21","chr22")
var.melt$chr <- factor(var.melt$chr, chrOrder, ordered=TRUE) 

p <- ggplot(var.melt, aes(variable, value))+
  geom_boxplot(outlier.shape = NA,aes(fill=variable))+
  theme_bw()+
  ylim(0,0.003)+
  ylab("Methylation variance")

p

ggsave(p,
       filename="meth.var.boxplot.pdf",
       width=12,
       height = 5)




