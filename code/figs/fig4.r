# Set path ----
.libPaths(c(.libPaths(),"/gpfs2/well/ludwig/users/cfo155/miniconda2/lib/R/library/"))
library(ComplexHeatmap)
library(circlize)
library(dplyr)
library(stringr)
library(RColorBrewer)
library(tidyr)
library(ggplot2)
library(pheatmap)
setwd("/users/ludwig/cfo155/cfo155/cfDNA/cfDNA_clean/cftaps_paper/plots/")
source("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/figs/code/functions.r")

# Functions ----
cal_allscores <- function(mat){
  alltrain <- NULL
  alltest <- NULL
  for(idx in 1:74){
    smpname <- data.frame(intest=mat[[idx]]$pred_test[1], intrain=mat[[idx]]['smp'] %>% unlist(), stringsAsFactors = FALSE)
    scores <- merge(smpname, do.call(cbind, mat)[3,idx] %>% as.data.frame(stringsAsFactors = FALSE),by.x="intrain",by.y=0) %>%
      merge(do.call(cbind, mat)[4,idx] %>% as.data.frame(stringsAsFactors = FALSE),by.x="intrain",by.y=0) %>%
      merge(do.call(cbind, mat)[5,idx] %>% as.data.frame(stringsAsFactors = FALSE),by.x="intrain",by.y=0) %>%
      merge(do.call(cbind, mat)[6,idx] %>% as.data.frame(stringsAsFactors = FALSE),by.x="intrain",by.y=0) %>%
      merge(do.call(cbind, mat)[7,idx] %>% as.data.frame(stringsAsFactors = FALSE),by.x="intrain",by.y=0) %>%
      merge(do.call(cbind, mat)[8,idx] %>% as.data.frame(stringsAsFactors = FALSE),by.x="intrain",by.y=0) %>%
      merge(do.call(cbind, mat)[9,idx] %>% as.data.frame(stringsAsFactors = FALSE),by.x="intrain",by.y=0) %>%
      merge(do.call(cbind, mat)[10,idx] %>% as.data.frame(stringsAsFactors = FALSE),by.x="intrain",by.y=0)
    alltrain <- rbind(alltrain, scores)
    alltest <- rbind(alltest, c(mat[[idx]]$pred_test[1],do.call(cbind, mat)[1,idx] %>% as.data.frame(stringsAsFactors = FALSE) %>% t()))
  }
  alltest <- as.data.frame(alltest,stringsAsFactors = FALSE)
  colnames(alltest) <- c("intrain","intest","status",
                         "cmb.HCC","cmb.Healthy","cmb.PDAC","cmb.predictted",
                         "glm_cv.HCC","glm_cv.Healthy","glm_cv.PDAC","glm_cv.predictted",
                         "knn_cv.HCC","knn_cv.Healthy","knn_cv.PDAC","knn_cv.predictted",
                         "rf_cv.HCC","rf_cv.Healthy","rf_cv.PDAC","rf_cv.predictted",
                         "svm_cv.HCC","svm_cv.Healthy","svm_cv.PDAC","svm_cv.predictted",
                         "glm.HCC","glm.Healthy","glm.PDAC","glm_predictted",
                         "rf.HCC","rf.Healthy","rf.PDAC","rf.predictted",
                         "svm.HCC","svm.Healthy","svm.PDAC","svm.predictted")
  alltrain$label <- "inTran"
  alltest$label <- "inTest"
  allscores <- rbind(alltrain,alltest[,grep("predictted|status",colnames(alltest),invert=TRUE)])
  return(allscores)
}

# Fig 4C & Fig S7A (Final prediction) ----
dat <- read.table("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/resource/cfDNA_cohort_1_sample_list_11302020.excured.newid.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)

methfile <- "/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/exclude_cured/3class_models/filtered.Regulatory_Build.selsmp.ws1000.agg.count.cpgnum.depth2.n5_5_5.diff0.3class.loo.pwt.excured.RData"
load(methfile)
meth <- res;rm(res)

tisffile <- "/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/3class/tiscon/sample_mixtures_oct/sample_mixtures_justEnhancers_MethAtlas_DMRs_bcells_tcells_neutrophils_dendriticells_separated_095_200_1000.3class.loo.excured.RData" 
load(tisffile)
tisf <- res;rm(res)

fragfile <- "/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/3class/frags/fracs/all.frag.length.sta.long.310-500.ratio.3class.loo.nocor.excured.RData"
load(fragfile)
frgf <- res;rm(res)

methscores <- cal_allscores(mat=meth)
tisfscores <- cal_allscores(mat=tisf)
frgfscores <- cal_allscores(mat=frgf)

colnames(methscores)[-c(1,2,ncol(methscores))] <- paste0("meth",colnames(methscores)[-c(1,2,ncol(methscores))])
colnames(tisfscores)[-c(1,2,ncol(tisfscores))] <- paste0("tisf",colnames(tisfscores)[-c(1,2,ncol(tisfscores))])
colnames(frgfscores)[-c(1,2,ncol(frgfscores))] <- paste0("frgf",colnames(frgfscores)[-c(1,2,ncol(frgfscores))])
allpred <- merge(methscores[methscores$label=="inTest",], tisfscores[tisfscores$label=="inTest",],by="intest") %>% merge(frgfscores[frgfscores$label=="inTest",],by="intest")

# use svm model 
selc <- "svm[^_]"
selpred <- allpred[,c(2,grep(selc,colnames(allpred)))]
selpred[,-1] <- apply(selpred[,-1],2,function(x)(as.numeric(as.character(x))))

selpred <- merge(dat %>% select(Sample_ID, Status), selpred, by.x="Sample_ID", by.y="intrain.x")
selpred$Status[selpred$Status=="Ctrl"] <- "Healthy"


# combine score (use mean(max))
selpred$mean_hcc <- apply(selpred[,grep("HCC",colnames(selpred))],1,mean)
selpred$mean_healthy <- apply(selpred[,grep("Healthy",colnames(selpred))],1,mean)
selpred$mean_pdac <- apply(selpred[,grep("PDAC",colnames(selpred))],1,mean)
selpred$max <- str_split_fixed(apply(selpred[,grep("mean",colnames(selpred))],1,
                                      function(x){colnames(selpred)[grep("mean",colnames(selpred))][which.max(x)]}) %>% as.character(),"_",2)[,2] %>% as.character()

selpred$pred_meth <- str_split_fixed(apply(selpred[,grep("^meth",colnames(selpred))],1,
                                           function(x){colnames(selpred)[grep("^meth",colnames(selpred))][which.max(x)]}) %>% as.character(),"\\.",2)[,2] %>% as.character()

selpred$pred_tisf <- str_split_fixed(apply(selpred[,grep("^tisf",colnames(selpred))],1,
                                           function(x){colnames(selpred)[grep("^tisf",colnames(selpred))][which.max(x)]}) %>% as.character(),"\\.",2)[,2] %>% as.character()

selpred$pred_frgf <- str_split_fixed(apply(selpred[,grep("^frgf",colnames(selpred))],1,
                                           function(x){colnames(selpred)[grep("^frgf",colnames(selpred))][which.max(x)]}) %>% as.character(),"\\.",2)[,2] %>% as.character()


selpred$pred_meth_t <- as.numeric(selpred$Status %>% tolower() == selpred$pred_meth %>% tolower())
selpred$pred_tisf_t <- as.numeric(selpred$Status %>% tolower() == selpred$pred_tisf %>% tolower())
selpred$pred_fraf_t <- as.numeric(selpred$Status %>% tolower() == selpred$pred_frgf %>% tolower())
selpred$max_t <- as.numeric(selpred$Status %>% tolower() == selpred$max %>% tolower())

sum(selpred$pred_meth_t==1)/74
sum(selpred$pred_tisf_t==1)/74
sum(selpred$pred_fraf_t==1)/74
sum(selpred$max_t==1)/74

# generate plot for each predictions
meth_cm <- table(selpred %>% select(Status,pred_meth)) %>% as.data.frame() %>% spread(Status, Freq)
meth_cm <- meth_cm[c(2,1,3),c(1,3,2,4)]
rownames(meth_cm) <- meth_cm[,1];meth_cm[,1]<-NULL
pheatmap(meth_cm,display_numbers=as.matrix(meth_cm),width=2,height=1.5,filename="meth_cm.pdf",cluster_rows = FALSE, cluster_cols = FALSE)

tisf_cm <- table(selpred %>% select(Status,pred_tisf)) %>% as.data.frame() %>% spread(Status, Freq)
tisf_cm <- tisf_cm[c(2,1,3),c(1,3,2,4)]
rownames(tisf_cm) <- tisf_cm[,1];tisf_cm[,1]<-NULL
pheatmap(tisf_cm,display_numbers=as.matrix(tisf_cm),width=2,height=1.5,filename="tisf_cm.pdf", cluster_rows = FALSE, cluster_cols = FALSE)

frgf_cm <- table(selpred %>% select(Status,pred_frgf)) %>% as.data.frame() %>% spread(Status, Freq)
frgf_cm <- frgf_cm[c(2,1,3),c(1,3,2,4)]
rownames(frgf_cm) <- frgf_cm[,1];frgf_cm[,1]<-NULL
pheatmap(frgf_cm,display_numbers=as.matrix(frgf_cm),width=2,height=1.5,filename="frgf_cm.pdf", cluster_rows = FALSE, cluster_cols = FALSE)


cmb_cm <- table(selpred %>% select(Status,max)) %>% as.data.frame() %>% spread(Status, Freq)
cmb_cm <- cmb_cm[c(2,1,3),c(1,3,2,4)]
rownames(cmb_cm) <- cmb_cm[,1];cmb_cm[,1]<-NULL
pheatmap(cmb_cm,display_numbers=as.matrix(cmb_cm),width=2,height=1.5,filename="cmb_cm.pdf", cluster_rows = FALSE, cluster_cols = FALSE)

selpredsta <- merge(do.call(cbind,aggregate(selpred$pred_meth_t, by=list(Category=selpred$Status), FUN = function(x) c(pred_meth_t_sum = sum(x), pred_meth_all = length(x) ) ) ) %>% as.data.frame(),
                    do.call(cbind,aggregate(selpred$pred_tisf_t, by=list(Category=selpred$Status), FUN = function(x) c(pred_tisf_t_sum = sum(x), pred_tisf_all = length(x) ) ) ) %>% as.data.frame(), by="Category")%>%
                    merge(do.call(cbind,aggregate(selpred$pred_fraf_t, by=list(Category=selpred$Status), FUN = function(x) c(pred_fraf_t_sum = sum(x), pred_fraf_all = length(x) ) ) ) %>% as.data.frame(),by="Category")%>%
                    merge(do.call(cbind,aggregate(selpred$max_t, by=list(Category=selpred$Status), FUN = function(x) c(max_t_sum = sum(x), max_t_all = length(x) ) ) ) %>% as.data.frame(),by="Category")
            
for(i in grep("sum",colnames(selpredsta))){
  selpredsta[,ncol(selpredsta)+1] <- as.numeric(as.character(selpredsta[,i]))/as.numeric(as.character(selpredsta[,i+1]))
  colnames(selpredsta)[ncol(selpredsta)] <- gsub("sum","pct",colnames(selpredsta)[i])
}
selpredsta.m <- selpredsta %>% select(Category,pred_meth_t_pct,pred_tisf_t_pct,pred_fraf_t_pct,max_t_pct) %>% reshape2::melt(id.vars=c("Category"))

selpredsta.m$Category <- factor(selpredsta.m$Category,levels=c("Healthy","HCC","PDAC"))
p <- ggplot(selpredsta.m,aes(x=Category,y=value,fill=Category)) +
  geom_bar(stat="identity",width=0.8) +
  mytheme + 
  facet_wrap(~variable , nrow=1) +
  scale_fill_manual(values=c("grey","#A73030FF","#0073C2")) +
  ylab("% sample classfied")
ggsave("labels_assigned.pdf",p,width=7,height = 3)

# Fig 4A & Fig S7B (Plot heatmap) ----
methfile <- "/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/exclude_cured/3class_models/filtered.Regulatory_Build.selsmp.ws1000.agg.count.cpgnum.depth2.n5_5_5.diff0.3class.loo.pwt.excured.RData"
load(methfile)
dmrs <- do.call(cbind, res)[11,] %>%  unlist() %>% table() %>% as.data.frame()
dmrs <- cbind(str_split_fixed(dmrs$.,"_",4)[,1:3],dmrs$Freq) %>% as.data.frame()
write.table(dmrs, gsub(".RData",".dmr.bed", methfile), col.names=FALSE, row.names=FALSE, quote=FALSE, sep="\t")

dmrfile <- "/gpfs2/well/ludwig/users/cfo155/cfDNA/022020_cfDNA/results/meth_correction/exclude_cured/3class_models/filtered.Regulatory_Build.selsmp.ws1000.agg.count.cpgnum.depth2.n5_5_5.diff0.3class.loo.pwt.excured.dmr.nearestgene.meth.txt"
dmrs <- read.table(dmrfile,sep="\t",header=TRUE,stringsAsFactors = FALSE)
dmrs$feature <- paste(dmrs$pos,dmrs$occurance, dmrs$nearestgene,dmrs$dis, sep = ":")
dmrs <- dmrs[,-grep("HCC_NHCCKAMI035",colnames(dmrs))]

dmrs.t <- t(dmrs[,grep("_rC",colnames(dmrs))]) %>% as.data.frame()
colnames(dmrs.t) <- dmrs$feature
rownames(dmrs.t) <- str_split_fixed(rownames(dmrs.t),"_",3)[,2]
dmrs.t[,which(colnames(dmrs.t)=="chr19_56301000_56302000:2:gene_name EDDM13;:0")] <- NULL
colnames(dmrs.t)[colnames(dmrs.t)%in%"chr19_56301000_56302000:2:gene_name ZSCAN5A;:0"] <- "chr19_56301000_56302000:2:gene_name ZSCAN5A_EDDM13;:0"
dmrs.t[,1:ncol(dmrs.t)] <- apply(dmrs.t,2,scale)


selc <- "frgfsvm[^_]|methsvm[^_]|tisfsvm[^_]"
selpred <- allpred[,c(2,grep(selc,colnames(allpred)))]
selpred[,-1] <- apply(selpred[,-1],2,function(x)(as.numeric(as.character(x))))
selpred <- merge(dat, selpred, by.x="Sample_ID", by.y="intrain.x")
selpred$TNM <- toupper(gsub("[a,b]|*N.*|T","",str_split_fixed(selpred$cancer_info,":",2)[,2]))
selpred <- merge(selpred,dmrs.t,by.x="Sample_ID",by.y=0)

rownames(selpred) <- selpred$idx
hccpred <- selpred[selpred$Status=="HCC",]
hccpred <- hccpred[order(-apply(hccpred[,grep("HCC",colnames(hccpred))],1,function(x){mean(as.numeric(as.character(x)))})),]
pdacpred <- selpred[selpred$Status=="PDAC",]
pdacpred <- pdacpred[order(-apply(pdacpred[,grep("PDAC",colnames(pdacpred))],1,function(x){mean(as.numeric(as.character(x)))})),]
healthypred <- selpred[selpred$Status=="Ctrl",]
healthypred <- healthypred[order(-apply(healthypred[,grep("Healthy",colnames(healthypred))],1,function(x){mean(as.numeric(as.character(x)))})),]


selpred <- rbind(healthypred,hccpred,pdacpred)
selpred$TNM[selpred$TNM==""] <- 0

selpred$pred_meth <- str_split_fixed(apply(selpred[,grep("^meth",colnames(selpred))],1,
                                           function(x){colnames(selpred)[grep("^meth",colnames(selpred))][which.max(x)]}) %>% as.character(),"\\.",2)[,2] %>% as.character()

selpred$pred_tisf <- str_split_fixed(apply(selpred[,grep("^tisf",colnames(selpred))],1,
                                           function(x){colnames(selpred)[grep("^tisf",colnames(selpred))][which.max(x)]}) %>% as.character(),"\\.",2)[,2] %>% as.character()

selpred$pred_frgf <- str_split_fixed(apply(selpred[,grep("^frgf",colnames(selpred))],1,
                                           function(x){colnames(selpred)[grep("^frgf",colnames(selpred))][which.max(x)]}) %>% as.character(),"\\.",2)[,2] %>% as.character()

selpred$Status[selpred$Status=="Ctrl"] <- "Healthy"
selpred$pred_meth_t <- as.numeric(selpred$Status %>% tolower() == selpred$pred_meth %>% tolower())
selpred$pred_tisf_t <- as.numeric(selpred$Status %>% tolower() == selpred$pred_tisf %>% tolower())
selpred$pred_fraf_t <- as.numeric(selpred$Status %>% tolower() == selpred$pred_frgf %>% tolower())
  
ha = HeatmapAnnotation(status=selpred$Status, meth=selpred$pred_meth_t, tisf=selpred$pred_tisf_t, frgf=selpred$pred_fraf_t,
                       col = list(
                         status = structure(names = c("Healthy", "HCC", "PDAC"),c("grey","#A73030","#0073C2")),
                         meth = structure(names= c("0","1"),c("#92C5DE","#F4A582")),
                         tisf = structure(names= c("0","1"),c("#92C5DE","#F4A582")),
                         frgf = structure(names= c("0","1"),c("#92C5DE","#F4A582"))
                       ))
col_fun = colorRamp2(c(seq(0,1,1/10)), rev(brewer.pal(11,"Spectral")))
pdf("tnm_scores.pdf", width = 12, height = 4)
Heatmap(t(selpred[,c(grep("Healthy",colnames(selpred)), grep("HCC",colnames(selpred)), grep("PDAC",colnames(selpred)))]), cluster_rows=FALSE,cluster_columns=FALSE, name = "scores",col=col_fun,top_annotation = ha)
dev.off()

selpos <- grep("chr",colnames(selpred),value=TRUE)[str_split_fixed(colnames(selpred)[grep("chr",colnames(selpred))],":",5)[,2] %>% as.numeric() %>% order(decreasing=TRUE)]
pdf("tnm_meth.pdf", width = 12, height = 10)
mat <- t(selpred[,c(colnames(selpred) %in% selpos)]) 
rownames(mat) <- paste0(str_split_fixed(rownames(mat),":",4)[,1],gsub("gene_name|;","",str_split_fixed(rownames(mat),":",4)[,3]))
Heatmap(mat, cluster_rows=TRUE,cluster_columns=FALSE, name = "Methylation",top_annotation = ha, col=colorRamp2(c(seq(-4,4,1)), rev(brewer.pal(9,"RdBu"))))
dev.off()

# Fig S7C (Enrichr plot) ----
dat <- read.table("/gpfs2/well/ludwig/users/cfo155/cfDNA/022020_cfDNA/figs/fig4/NCI-Nature_2015_table_filtered.Regulatory_Build.selsmp.ws1000.agg.count.cpgnum.depth2.n5_5_5.diff0.3class.loo.pwt.excured.dmr.nearestgene.txt", header=TRUE, sep="\t")
dat$logp <- -log(dat$P.value,10)
dat$term <- gsub(" Homo sapiens.*","",dat$Term)
dat$term <- factor(dat$term, levels=dat[order(-dat$P.value),]$term)
p <- ggplot(dat[dat$P.value<0.05,],aes(x=term,y=logp)) +
  geom_bar(stat="identity",fill="grey") +
  theme(axis.text.x = element_text(angle = 45, vjust = 0.5, hjust=1))+ coord_flip() + mytheme
ggsave("NCI-Nature_2015_table_filtered.Regulatory_Build.selsmp.ws1000.agg.count.cpgnum.depth2.n5_5_5.diff0.3class.loo.pwt.excured.dmr.nearestgene.pdf",p,width=5,height=4)




