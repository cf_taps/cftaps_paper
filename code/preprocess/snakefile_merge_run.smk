"""
Workflow to map sequencing data from 3 runs to the genome and mark duplicates
inputs: 
    fastq
outputs:
    mods
run:
conda activate snakemake
snakemake -np --use-envmodules --max-status-checks-per-second 0.01 -j 48 --snakefile snakefile_run_cftaps.merge.smk --cluster "qsub -o cftaps.log -e cftaps.err -cwd -V -S /bin/bash -N cftaps -j y -V -l h_rt=20:00:00 -l h_vmem=15G"
"""

configfile: "/users/jcheng/jcheng/cfTAPS/process/configure.yaml"

from snakemake.utils import min_version
min_version("5.26")

MERGED_SAMPLES = config["MERGED_SAMPLES"]
print(MERGED_SAMPLES)
REF = "resource/GRCh38_spike_in.carrier_DNA.fa"
REF_SIZE = "resource/GRCh38_spike_in.carrier_DNA.fa.fai"



rule all:
    input:
        # expand("align/{merge}.md.bam", merge=MERGED_SAMPLES),
        # expand("meth/{merge}_clip10_merge_CpG.bedGraph", merge=MERGED_SAMPLES),
        # expand("meth/{merge}_clip10_merge_CpG.filter_taps.bedGraph", merge=MERGED_SAMPLES),
        expand("align/{merge}.spikein.sort.bam", merge=MERGED_SAMPLES),
        expand("meth/{merge}.spikein_clip10_CpG.bedGraph", merge=MERGED_SAMPLES),
        expand("sta/{merge}_clip10.sta", merge=MERGED_SAMPLES)


rule merge_bam:
    input:
        expand("align/{{merge}}_run{run}.md.bam", run=['1','2','3'])
    output:
        mgbam="align/{merge}.bam",
        stbam="align/{merge}.sort.bam",
        mdbam="align/{merge}.md.bam",
        matrix="align/{merge}.md.matrix.txt"
    params:
        prefix="align/{merge}"
    shell:
        """
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools merge {output.mgbam} {input}
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools sort -@ 2 -m 1G -T  {output.mgbam} -o {output.stbam}
        /mnt/lustre/users/jcheng/tools/miniconda2/share/picard-2.25.2-0/picard MarkDuplicates \
            I={output.stbam} \
            O={output.mdbam} \
            M={output.matrix}
        """

rule MethylDackel_extract_genome_merge:
    input:
        "align/{merge}.md.bam"
    output: 
        "meth/{merge}_clip10_merge_CpG.bedGraph"
    params:
        pars="-q 10 -p 13 -t 2 --OT 10,140,10,140 --OB 10,140,10,140 --mergeContext ",
        prefix="meth/{merge}_clip10_merge",
        ref=REF
    envmodules: 
        "matplotlib/3.1.1-foss-2019b-Python-3.7.4"
    shell: 
        """
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/MethylDackel extract \
            {params.ref} {input} \
            {params.pars} -o {params.prefix} 
        """

rule taps_meth:
    input:
        "meth/{merge}_clip10_merge_CpG.bedGraph"
    output: 
        "meth/{merge}_clip10_merge_CpG.filter_taps.bedGraph"
    params:
        excludregion="resource/hg38-blacklist.v2.bed resource/hg38.centromers.bed resource/dbSNP154.bed"
    shell: 
        """
        cat {input}|awk 'BEGIN{{OFS="\t"}}{{print $1,$2,$3,100-$4,$6,$5+$6}}' |sort -k1,1 -k2,2n|\
            intersectBed -a - -b <(cat {params.excludregion}| grep -P "chr[0-9,X,Y,M]*\t"|cut -f1-3|sort -k1,1 -k2,2n ) -v -sorted >{output}
        """


rule extract_spikein:
    input:
        expand("align/{{merge}}_run{run}.bwa_nofilter.bam", run=['1','2','3'])
    output:
        spbam=temp(expand("align/{{merge}}_run{run}.spikein.bam", run=['1','2','3'])),
        mgbam=temp("align/{merge}.spikein.merge.bam"),
        stbam="align/{merge}.spikein.sort.bam"
    params:
        prefix="align/{merge}.spikein",
        ref_size=REF_SIZE
    shell:
        """
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools view -q 1 -bS -L <(cat {params.ref_size} |grep -v ^chr|awk 'BEGIN{{OFS="\t"}}{{print $1,"0",$2}}') {input[0]} >{output.spbam[0]}
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools view -q 1 -bS -L <(cat {params.ref_size} |grep -v ^chr|awk 'BEGIN{{OFS="\t"}}{{print $1,"0",$2}}') {input[1]} >{output.spbam[1]}
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools view -q 1 -bS -L <(cat {params.ref_size} |grep -v ^chr|awk 'BEGIN{{OFS="\t"}}{{print $1,"0",$2}}') {input[2]} >{output.spbam[2]}
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools merge {output.mgbam} {output.spbam}
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools sort -@ 2 -m 1G -T {params.prefix} {output.mgbam} -o {output.stbam}
        """

rule MethylDackel_extract_spikein:
    input:
        "align/{merge}.spikein.sort.bam"
    output: 
        expand("meth/{{merge}}.spikein_clip10_{context}.bedGraph", context=['CpG','CHG','CHH'])
    params:
        pars="-q 10 -p 13 -t 2 --CHG --CHH --OT 10,140,10,140 --OB 10,140,10,140",
        prefix="meth/{merge}.spikein_clip10",
        ref=REF,
        ref_size=REF_SIZE
    shell: 
        """
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/MethylDackel extract \
            {params.ref} {input} \
            {params.pars} -l <(cat {params.ref_size} |grep -v ^chr|awk 'BEGIN{{OFS="\t"}}{{print $1,"0",$2}}') -o {params.prefix} 
        """

rule meth_sta:
    input:
        expand("meth/{{merge}}.spikein_clip10_{context}.bedGraph", context=['CpG','CHG','CHH'])
    output: 
        "sta/{merge}_clip10.sta"
    shell: 
        """
        mCG_2k=`cat {input[0]}| awk -F "\t" '$1=="unmodified_2kb" && $5+$6>0' |awk -F "\t" '{{mC+=$6;uC+=$5}}END{{printf("%.4f", mC/(mC+uC))}}'`
        nCG_2k=`cat {input[0]}| awk -F "\t" '$1=="unmodified_2kb" && $5+$6>0' |awk -F "\t" '{{mC+=$6;uC+=$5}}END{{printf("%.4f", (mC+uC)/NR)}}'`
        mCG_lambda=`cat {input[0]}|awk -F "\t" '$1=="J02459.1" && $5+$6>0' |awk -F "\t" '{{mC+=$6;uC+=$5}}END{{printf("%.4f", mC/(mC+uC))}}'`
        nCG_lambda=`cat {input[0]}|awk -F "\t" '$1=="J02459.1" && $5+$6>0' |awk -F "\t" '{{mC+=$6;uC+=$5}}END{{printf("%.4f", (mC+uC)/NR)}}'`
        
        mCH_2k=`cat {input[1]} {input[2]} |awk -F "\t" '$1=="unmodified_2kb" && $5+$6>0'|awk -F "\t" '{{mC+=$6;uC+=$5}}END{{printf("%.4f", mC/(mC+uC))}}'`
        nCH_2k=`cat {input[1]} {input[2]} |awk -F "\t" '$1=="unmodified_2kb" && $5+$6>0'|awk -F "\t" '{{mC+=$6;uC+=$5}}END{{printf("%.4f", (mC+uC)/NR)}}'`
        mCH_lambda=`cat {input[1]} {input[2]} |awk -F "\t" '$1=="J02459.1" && $5+$6>0'|awk -F "\t" '{{mC+=$6;uC+=$5}}END{{printf("%.4f", mC/(mC+uC))}}'`
        nCH_lambda=`cat {input[1]} {input[2]} |awk -F "\t" '$1=="J02459.1" && $5+$6>0'|awk -F "\t" '{{mC+=$6;uC+=$5}}END{{printf("%.4f", (mC+uC)/NR)}}'`
        echo -e "mCG_lambda\tnCG_lambda\tmCG_2k\tnCG_2k\tmCH_2k\tnCH_2k\tmCH_lambda\tnCH_lambda" >{output}
        echo -e "${{mCG_lambda}}\t${{nCG_lambda}}\t${{mCG_2k}}\t${{nCG_2k}}\t${{mCH_2k}}\t${{nCH_2k}}\t${{mCH_lambda}}\t${{nCH_lambda}}"  >>{output}
        """
