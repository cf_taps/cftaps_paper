import pandas as pd
import numpy as np
import seaborn as sns
import os
import re
from scipy import optimize
import fastcluster
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import pickle as pkl
from matplotlib.lines import Line2D
import collections
from collections import defaultdict
from adjustText import adjust_text
from sklearn.manifold import TSNE


## Define functions for making tissue map

#### Make Ratio DF
def get_ratio_df(df,subset_columns=[]):
    ''' 
    Separates input df into total, methylated and methylation_ratio dfs, and outputs all 3.
    Handles numpy error state to avoid zero division errors if total_df contains zeros.
    '''
    total_df = df.loc[:,[u for u in list(df.columns) if 'Total' in u]]
    meth_df = df.loc[:,[u for u in list(df.columns) if 'Meth' in u]]
    names = [u.replace('Total_','') for u in list(df.columns) if 'Total' in u]
    meth_array=np.array(meth_df)
    total_array=np.array(total_df)
    with np.errstate(divide='ignore'):
        ratio_array = meth_array/total_array
        ratio_array[total_array==0] = 0
    ratio_df = pd.DataFrame(ratio_array)
    ratio_df.columns = names
    total_df.columns = names
    meth_df.columns = names
    ratio_df = ratio_df.set_index(df.index)
    total_df = total_df.set_index(df.index)
    meth_df = meth_df.set_index(df.index)
    if len(subset_columns) > 0:
        ratio_df = ratio_df.loc[:,subset_columns]
        total_df = total_df.loc[:,subset_columns]
        meth_df = meth_df.loc[:,subset_columns]
    return ratio_df,total_df,meth_df


def make_tissue_average_df(df,tissue_groups_id_dict,median=False):
    tissue_ordered_df = df.copy()
    tissue_group_meth_dict = {}
    tissue_group_total_dict = {}
    tissue_group_dict = {}
    tissue_order = []
    for group in tissue_groups_id_dict.keys():
        if ('Meth' in df.columns) | ('Total' in df.columns):
            print('bla')
            available_tissue_group_cols_meth = [f'Meth_{u}' for u in tissue_groups_id_dict[group] if f'Meth_{u}' in tissue_ordered_df.columns]
            available_tissue_group_cols_total = [f'Total_{u}' for u in tissue_groups_id_dict[group] if f'Total_{u}' in tissue_ordered_df.columns]

            if len(available_tissue_group_cols_meth) > 0:
                tissue_group_meth = tissue_ordered_df.loc[:,available_tissue_group_cols_meth]
                tissue_group_total = tissue_ordered_df.loc[:,available_tissue_group_cols_total]
                tissue_group_dict[f'Total_{group}'] = tissue_group_total.mean(axis=1).values
                tissue_group_dict[f'Meth_{group}'] = tissue_group_meth.mean(axis=1).values
        else:
            available_tissue_group_cols = [u for u in tissue_groups_id_dict[group] if u in tissue_ordered_df.columns]
            if len(available_tissue_group_cols) > 0:
                tissue_group = tissue_ordered_df.loc[:,available_tissue_group_cols]
                if median == True:
                    tissue_group_dict[group] = tissue_group.median(axis=1).values
                else:
                    tissue_group_dict[group] = tissue_group.mean(axis=1).values
    tissue_group_means = pd.DataFrame.from_dict(tissue_group_dict)
    tissue_group_means['chr_start_end'] = tissue_ordered_df.index
    tissue_group_means = tissue_group_means.set_index('chr_start_end')
    return tissue_group_means


def meth_atlas_dmrs(df,median_diff_quantile,n_pairwise_dmrs,tissue_groups_id_dict):

    ## Order tissues by group
    tissue_group_order = []
    for tissue in tissue_groups_id_dict.keys():
        tissue_group_order.extend(tissue_groups_id_dict[tissue])
    avail_tissue_group_order = [u for u in tissue_group_order if u in df.columns]

    df = df[avail_tissue_group_order]

    ## Find plural tissues
    lone_tissue_list = []
    plural_tissues = []
    for tissue in tissue_groups_id_dict.keys():
        avail_tissue_ids = [u for u in tissue_groups_id_dict[tissue] if u in df.columns]
        if len(avail_tissue_ids) == 1:
            lone_tissue_list.append(tissue)
        elif len(avail_tissue_ids) > 1:
            plural_tissues.append(tissue)

    selected_regions = pd.DataFrame([])
    for selected_tissue in plural_tissues:
        avail_tissue_columns = [u for u in tissue_groups_id_dict[selected_tissue] if u in df]
        remaining_avail_tissue_columns = [u for u in df if u not in avail_tissue_columns]
        tissue_group = df.loc[:,avail_tissue_columns]
        rest = df.loc[:,remaining_avail_tissue_columns]
        tissue_df = df.copy(deep=True)
        tissue_df['tissue_median'] = tissue_group.median(axis=1)
        tissue_df['group_median'] = rest.median(axis=1)
        tissue_df['median_difference'] = np.abs(tissue_group.median(axis=1) - rest.median(axis=1))
        tissue_df['tissue_mean'] = tissue_group.mean(axis=1)
        tissue_df['group_mean'] = rest.mean(axis=1)
        tissue_df['mean_difference'] = np.abs(tissue_group.mean(axis=1) - rest.mean(axis=1))
        tissue_df['Hyper_or_Hypo'] = ['Hyper' if u==True else 'Hypo' for u in (np.array(tissue_group.median(axis=1) - rest.median(axis=1) > 0))]
        tissue_df['tissue_variance'] = tissue_group.var(axis=1) 
        tissue_df['group_variance'] = rest.var(axis=1)
        tissue_df['variance_difference'] = rest.var(axis=1) - tissue_group.var(axis=1)
        
        ## Select regions with largest median difference, then sort by tissue_variance
        top_regions = tissue_df[(tissue_df['median_difference'] >tissue_df['median_difference'].quantile(median_diff_quantile) )]
        if top_n_tissue_variance:
            top_regions = top_regions.sort_values(by='tissue_variance',ascending=True).head(top_n_tissue_variance)
        if top_n_group_variance:
            top_regions = top_regions.sort_values(by='group_variance',ascending=True).head(top_n_group_variance)
        top_regions['selected_tissue'] = selected_tissue
        selected_regions = selected_regions.append(top_regions)



    # Now add DMRs that differentiate two closest tissues pairwise
    tissue_group_means = make_tissue_average_df(selected_regions.loc[:,df.columns],tissue_groups_id_dict)

    # Use correlation matrix to find most highly correlated df
    tissue_group_means_corr = tissue_group_means.corr()
    pairwise_most_correlated_dict = {}
    pairwise_second_most_correlated_dict = {}
    pairwise_correlated_dmr_indices = []
    tissue_list = []
    for tissue in tissue_group_means_corr.columns:
        ## Find most closely correlated tissue
        most_correlated = tissue_group_means_corr.sort_values(by=tissue,ascending=False).iloc[1,:].name
        pairwise_most_correlated_dict[tissue] = most_correlated

        ## Now find DMR with largest median difference between the pair 
        median_difference_pair = np.abs(tissue_group_means.loc[:,tissue] - tissue_group_means.loc[:,most_correlated])
        median_difference_pair = median_difference_pair.sort_values(ascending=False)
        pairwise_correlated_dmr_indices.extend(list(median_difference_pair.head(n_pairwise_dmrs).index))
        tissue_list.extend([tissue]*n_pairwise_dmrs)
        
        
    ## Add DMRs that differentiate second most correlated tissue and liver
        second_most_correlated = tissue_group_means_corr.sort_values(by=tissue,ascending=False).iloc[2,:].name
        pairwise_second_most_correlated_dict[tissue] = second_most_correlated
        ## Now find DMR with largest median difference between the pair 
        median_difference_pair = np.abs(tissue_group_means.loc[:,tissue] - tissue_group_means.loc[:,second_most_correlated])
        median_difference_pair = median_difference_pair.sort_values(ascending=False)
        pairwise_correlated_dmr_indices.extend(list(median_difference_pair.head(n_pairwise_dmrs).index))
        tissue_list.extend([tissue]*n_pairwise_dmrs)


    ## Remove all cols except sample_id and selected_tissue from selected_regions
        selected_regions = selected_regions.loc[:,list(df.columns) + ['selected_tissue']]
        new_pairwise_dmrs = df.loc[pairwise_correlated_dmr_indices,:].copy(deep=True)
        new_pairwise_dmrs['selected_tissue'] = tissue_list

        selected_regions = selected_regions.append(new_pairwise_dmrs.drop_duplicates())

    return selected_regions,pairwise_most_correlated_dict,pairwise_second_most_correlated_dict

def map_samples_to_rows(list_of_names,all_samples):
    positions = [all_samples.index(u) for u in list_of_names]
    return positions

# -----------------------------------          Load tissue atlas         -----------------------------------------------
meth_atlas_tissues = pd.read_csv('path/to/tissue_atlas',sep='\t').set_index('chr_start_end')
ratio_df,total_df,meth_df = get_ratio_df(meth_atlas_tissues)

# -----------------------------------          Filter Atlas         -----------------------------------------------
# Count zero coverage rows in each tissue sample
zero_coverage_rows = (total_df == 0).sum(axis=0)
low_coverage_list = list(zero_coverage_rows[zero_coverage_rows > zero_coverage_rows.quantile(0.95)].index)

# Remove sex-specific tissues, progenitor cell types and cell culture
tissue_groups_to_remove = ['breast','ovary','testis','prostate','common myeloid progenitor, CD34-positive',\
                            'culture','Neural progenitor cell']
tissue_samples_to_remove = [tissue_groups_id_dict[u] for u in tissue_groups_to_remove] 
## Flatten nested list
tissue_samples_to_remove = [u for group in tissue_samples_to_remove for u in group]+ low_coverage_list

filtered_samples = [u for u in ratio_df.columns if u not in tissue_samples_to_remove]
ratio_df = ratio_df.loc[:,filtered_samples]
total_df = total_df.loc[:,filtered_samples]
meth_df = meth_df.loc[:,filtered_samples]
filtered_columns = ['Total_' + u for u in filtered_samples] + ['Meth_' + u for u in filtered_samples]
meth_atlas_tissues_filtered = meth_atlas_tissues.loc[:,filtered_columns]
# Remove rows where coverage = 0
meth_atlas_tissues_filtered = meth_atlas_tissues_filtered.loc[(np.sum((total_df==0),axis=1)==0)]
                           
ratio_df,total_df,meth_df = get_ratio_df(meth_atlas_tissues_filtered)





# -----------------------------------     Load already filtered Samples         -----------------------------------------------
samples_df = pd.read_csv('path/to/cfdna_samples/',sep='\t')
samples_df['chr_start_end'] = ['_'.join(u.split('_')[0:3]) for u in samples_df['pos_cpg']]
n_cpgs_per_region = [u.split('_')[3] for u in samples_df['pos_cpg']]
samples_df = samples_df.set_index('chr_start_end')
samples_df = samples_df.drop(columns='pos_cpg')
column_rename_dict = {u: u.replace('_rC','') for u in samples_df.columns}
samples_df = samples_df.rename(columns=column_rename_dict)

samples_n_cpgs_df = pd.DataFrame([])
samples_n_cpgs_df['n_cpgs'] = n_cpgs_per_region
samples_n_cpgs_df = samples_n_cpgs_df.set_index(samples_df.index)



# -----------------------------------     Merge Tissue Atlas and Samples         -----------------------------------------------
merged_df = meth_atlas_tissues_filtered.merge(samples_df,on='chr_start_end')
merged_tissues = merged_df.loc[:,meth_atlas_tissues_filtered.columns]
samples_df = merged_df.loc[:,samples_df.columns]
merged_tissues_ratio,merged_tissues_total,merged_tissues_meth = get_ratio_df(merged_tissues)

# Filter samples_n_cpgs_df to include same regions, then include tissue coverage
samples_n_cpgs_df = samples_n_cpgs_df.loc[merged_samples_ratio.index,:]
samples_n_cpgs_df['tissue_coverage'] = merged_tissues_total.median(axis=1)
samples_n_cpgs_df['normalised_coverage'] = samples_n_cpgs_df['tissue_coverage'].astype(int)/samples_n_cpgs_df['n_cpgs'].astype(int)

atlas_df = make_tissue_average_df(merged_tissues_ratio,tissue_groups_id_dict)
selected_regions,pairwise_most_correlated,pairwise_second_most_correlated = meth_atlas_dmrs(merged_tissues_ratio,0.95,200,tissue_groups_id_dict,near_zero_or_one=True,solid_only=False,include_second_most_correlated_dmrs=True)
atlas_df = atlas_df.loc[selected_regions.index,:]
samples_df = samples_df.loc[selected_regions.index,:]


# -----------------------------------    NNLS Deconvolution        -----------------------------------------------
sample_mixtures = pd.DataFrame([])
residuals = []
for sample_name in samples_df:
    mixture, residual = optimize.nnls(atlas_df, samples_df.loc[:,sample_name])
    mixture /= np.sum(mixture)
    residuals.append(residual)
    sample_mixtures[sample_name] = mixture 

## Tissue_Group_Means df has colnames = tissue, so can just assign tissue
sample_mixtures['atlas_tissues'] = atlas_df.columns
sample_mixtures = sample_mixtures.set_index('atlas_tissues')
mean_residual = np.array(residuals).mean()
normalised_residual = np.array(residuals).mean()/merged_tissues_ratio.shape[0]






## ----------------            t-SNE plot of tissue atlas              ------------------------


pca_df = merged_tissues_ratio
tissue_array = np.array(pca_df.transpose())

#### -----------------------    Choose embedding method       ------------------------

## PCA
# pca = PCA(n_components=2)
# Xtrans = pca.fit_transform(tissue_array)


## t-SNE
Xtrans = TSNE(n_components=2).fit_transform(tissue_array)

# U-MAP
# reducer = umap.UMAP()
# Xtrans = reducer.fit_transform(tissue_array)


fig = plt.figure(figsize=(16,12))


## Plot Xtrans in parts, so that different samples can be grouped together by df_origin 
## Looping through df_origin.keys() object


## Configure Plot   
distinct_markers = ['1','o','+','H','s','p','*','d','4']*10
cmap = plt.get_cmap('jet')
distinct_colors = cmap(np.linspace(0, 1.0, len(looping_dict)))
k = 0
 
annotations= []
already_annotated = []
for key in [key for key in looping_dict.keys() if key not in tissue_groups_to_remove]:
# for origin in df_origin.keys():
                           
    # Get remaining (non-filtered) positions of each origin group
    positions = map_samples_to_rows([u for u in looping_dict[key] if u in list(pca_df.columns)],list(pca_df.columns))
    plt.scatter(Xtrans[positions,0],Xtrans[positions,1],marker=distinct_markers[k],color=distinct_colors[k],label=key)
    for position in positions:
        if key not in already_annotated:
            annotations.append(plt.text(Xtrans[position,0],Xtrans[position,1],key))
            already_annotated.append(key)
                           
    k+=1
adjust_text(annotations)   