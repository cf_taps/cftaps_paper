"""
Workflow to map sequencing data from 4 runs to the genome and mark duplicates
inputs: 
    fastq
outputs:
    mods
run:
conda activate snakemake
snakemake -np --use-envmodules --max-status-checks-per-second 0.01 -j 48 --snakefile snakefile_run_cftaps.smk --cluster "qsub -o cftaps.log -e cftaps.err -cwd -V -S /bin/bash -N cftaps -j y -V -l h_rt=20:00:00 -l h_vmem=15G"
"""
from snakemake.utils import min_version
min_version("5.26")


configfile: "configure.yaml"
MERGED_SAMPLES = config["MERGED_SAMPLES"]
print(MERGED_SAMPLES)

SAMPLES = expand("{mergedsample}_run{run}", run=['1','2','3'],mergedsample = MERGED_SAMPLES)
REF = "resource/GRCh38_spike_in.carrier_DNA.fa"
REF_SIZE = "resource/GRCh38_spike_in.carrier_DNA.fa.fai"


rule all:
    input:
        ## Processing different lane
        expand("fastq/{sample}_R{readDirection}_val_{readDirection}.fq.gz", readDirection=['1','2'],sample = SAMPLES), 
        expand("fastq/{sample}_R{readDirection}_val_{readDirection}_fastqc.html", readDirection=['1','2'],sample = SAMPLES),
        # expand("align/{sample}.bwa.bam", sample = SAMPLES),
        expand("align/{sample}.md.bam", sample = SAMPLES),
        expand("align/{sample}.map.txt", sample = SAMPLES),
        expand("align/{sample}.bwa.map.txt", sample = SAMPLES),
        expand("align/{sample}.bwa.uniqmap.txt", sample = SAMPLES),
        expand("align/{sample}.md.uniqmap.txt", sample = SAMPLES),
        expand("align/{sample}.bwa_nofilter.bam", sample = SAMPLES),
        expand("align/{sample}.unmapped_r{readDirection}.fq.gz", sample = SAMPLES, readDirection=['1','2'])

        
rule trim_galore_pe: 
    input:
        expand("rawdata/{{sample}}_{readDirection}.fastq.gz",readDirection=['1','2'])
    output:
        expand("fastq/{{sample}}_R{readDirection}_val_{readDirection}.fq.gz", readDirection=['1','2'])
    log:
        "logs/{sample}.trim.log"
    params:
        out_path="fastq/",
        out_fix="{sample}"
    threads: 3
    shell:
        """
        (/mnt/lustre/users/jcheng/tools/miniconda2/bin/trim_galore \
        --path_to_cutadapt /mnt/lustre/users/jcheng/tools/miniconda2/bin/cutadapt \
        --paired --length 35 --gzip --cores {threads} \
        --fastqc -o {params.out_path} {input} --basename {params.out_fix}) 2> {log}
        """

rule fastqc: 
    input:
        expand("fastq/{{sample}}_R{readDirection}_val_{readDirection}.fq.gz", readDirection=['1','2'])
    output:
        expand("fastq/{{sample}}_R{readDirection}_val_{readDirection}_fastqc.html", readDirection=['1','2'])
    log:
        "logs/{sample}.fastqc.log"
    params:
        out_path="fastq/",
        out_fix="{sample}"
    threads: 3
    shell:
        """
        /mnt/lustre/users/jcheng/tools/miniconda2/opt/fastqc-0.11.9/fastqc {input[0]}
        /mnt/lustre/users/jcheng/tools/miniconda2/opt/fastqc-0.11.9/fastqc {input[1]}
        """


rule align: 
    input:
        expand("fastq/{{sample}}_R{readDirection}_val_{readDirection}.fq.gz", readDirection=['1','2'])
    output:
        "align/{sample}.bwa.bam"
    log:
        "logs/{sample}.bwa.log"
    params:
        ref=REF,
        tmp="{sample}"
    threads: 2
    shell:
        """
        (
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/bwa mem -t {threads} {params.ref} {input} -I 500,120,1000,20 |\
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools view -q 1 -O BAM |
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools sort -@ 8 -O BAM -T {params.tmp} >{output}
        ) 1>{log} 2>&1
        """

rule markdup_each: 
    input:
        "align/{sample}.bwa.bam"
    output:
        mdbam="align/{sample}.md.bam",
        matrix="align/{sample}.md.matrix.txt"
    log:
        "logs/{sample}.picard.log"
    params:
        mdtmp=temp("markdup") 
    shell:
        """
        (
            /mnt/lustre/users/jcheng/tools/miniconda2/share/picard-2.25.2-0/picard MarkDuplicates \
            I={input} \
            O={output.mdbam} \
            M={output.matrix} \
            TMP_DIR={params.mdtmp}) 1>{log} 2>&1
        """


rule map_sta_md: 
    input:
        "align/{sample}.md.bam"
    output:
        "align/{sample}.map.txt"
    shell:
        """
        nmap=`/mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools view {input} |cut -f1 |sort -u |wc -l`
        nuniqmap=`/mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools view {input} |awk '$2==99 || $2==83'|cut -f1 |sort -u |wc -l`
        nuniqmap_sep=`/mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools view {input} |awk '$2==99 || $2==83'|cut -f1,3|sort -u|cut -f2|sort |uniq -c|\
            sed 's/^ *//g'|sed 's/ /\t/g'|awk 'BEGIN{{FS="\t";OFS="\t"}}{{if($2~/chr/)print $1,"hg38";else if($2=="pNIC28-Bsa4:240-2245") print $1,"carrier"; else print $1,"spike-in"}}'|\
            awk '{{sum[$2]+=$1}}END{{for(i in sum)print i"\t"sum[i]}}'|sort -k1,1 |\
            paste - - - |awk 'BEGIN{{OFS="\t"}}{{print $1,$2,$3,$4,$5,$6,($2+$4+$6),$2/($2+$4+$6)}}'`
        echo -e "${{nmap}}\t${{nuniqmap}}\t${{nuniqmap_sep}}" > {output}
        """

rule map_sta_nomd:
    input:
        "align/{sample}.bwa.bam"
    output:
        "align/{sample}.bwa.map.txt"
    shell:
        """
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools view {input} |cut -f1,3|sort -u|cut -f2|sort |uniq -c|\
        sed 's/^ *//g'|sed 's/ /\t/g'|awk 'BEGIN{{FS="\t";OFS="\t"}}{{if($2~/chr/)print $1,"hg38";else if($2=="pNIC28-Bsa4:240-2245") print $1,"carrier"; else print $1,$2}}'  |awk '{{sum[$2]+=$1}}END{{for(i in sum)print i"\t"sum[i]}}'|sort -k1,1 > {output}
        """

rule map_sta_nomd_uniq:
    input:
        "align/{sample}.bwa.bam"
    output:
        "align/{sample}.bwa.uniqmap.txt"
    shell:
        """
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools view {input} |grep -v -e 'XA:Z:' -e 'SA:Z:' |cut -f1,3|sort -u|cut -f2|sort |uniq -c|\
        sed 's/^ *//g'|sed 's/ /\t/g'|awk 'BEGIN{{FS="\t";OFS="\t"}}{{if($2~/chr/)print $1,"hg38";else if($2=="pNIC28-Bsa4:240-2245") print $1,"carrier"; else print $1,$2}}'  |awk '{{sum[$2]+=$1}}END{{for(i in sum)print i"\t"sum[i]}}'|sort -k1,1 > {output}
        """

rule map_sta_md_uniq:
    input:
        "align/{sample}.md.bam"
    output:
        "align/{sample}.md.uniqmap.txt"
    shell:
        """
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools view {input} |awk '$2==99 || $2==83'|grep -v -e 'XA:Z:' -e 'SA:Z:' |cut -f1,3|sort -u|cut -f2|sort |uniq -c|\
        sed 's/^ *//g'|sed 's/ /\t/g'|awk 'BEGIN{{FS="\t";OFS="\t"}}{{if($2~/chr/)print $1,"hg38";else if($2=="pNIC28-Bsa4:240-2245") print $1,"carrier"; else print $1,$2}}'  |awk '{{sum[$2]+=$1}}END{{for(i in sum)print i"\t"sum[i]}}'|sort -k1,1 > {output}
        """

rule unmapped_fastq:
    input:
            bam="align/{sample}.bwa_nofilter.bam",
            fq=expand("fastq/{{sample}}_R{readDirection}_val_{readDirection}.fq.gz", readDirection=['1','2'])
    output:
            readid="align/{sample}.id",
            fq1="align/{sample}.unmapped_r1.fq.gz",
            fq2="align/{sample}.unmapped_r2.fq.gz"
    log:
            "logs/{{sample}}.unmapped_fq.log"
    shell:
        """
        (
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools view -f4 {input.bam} |cut -f1 |sort -u >{output.readid}
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/seqtk subseq {input.fq[0]} {output.readid} |gzip - > {output.fq1}
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/seqtk subseq {input.fq[1]} {output.readid} |gzip - > {output.fq2}
        ) 1>{log} 2>&1
        """

rule align_q0:
    input:
        expand("fastq/{{sample}}_R{readDirection}_val_{readDirection}.fq.gz", readDirection=['1','2'])
    output:
        "align/{sample}.bwa_nofilter.bam"
    log:
        "logs/{sample}.bwa_nofilter.log"
    params:
        ref=REF
    threads: 4
    shell:
        """
        (
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/bwa mem -t {threads} {params.ref} {input} -I 500,120,1000,20 |
        /mnt/lustre/users/jcheng/tools/miniconda2/bin/samtools view -bS - -o {output}
        ) 1>{log} 2>&1
        """
