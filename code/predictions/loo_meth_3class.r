#!/mnt/lustre/users/jcheng/tools/miniconda2/bin/Rscript --vanilla

# Specify a job name
#$ -N dmrpredict

# Run the job in the current working directory
#$ -cwd -j y -V
#$ -l h_rt=80:00:00
#$ -l h_vmem=8G
# Parallel environemnt settings
#$ -pe smp 1
# Some useful data about the job to help with debugging

.libPaths(c(.libPaths(),"/users/jcheng/R/x86_64-pc-linux-gnu-library/3.6","/mnt/lustre/users/jcheng/tools/miniconda2/lib/R/library","/users/jcheng/gridware/share/R/3.6.2"))

library(readr)
library(tidyverse)
library(reshape2)
library(dplyr)
library(caret)
library(glmnet)
library(ROCR)
library(ggplot2)
library(pryr)
library(cowplot)
library(ggplotify)
library(RColorBrewer)
library(MLmetrics)
library(GenomicRanges)
library(plotROC)
library(pheatmap)
library(pROC)
library(randomForest)

source("code/functions.r")

mlmethods <-  function(inTrain, inTest,mlmodel="rf"){
  ### build model ##
  # glm #
  control <- trainControl(method='repeatedcv', number=5, repeats=3,search='grid')
  tunegrid <- expand.grid(alpha = 0:1,lambda = seq(0.01,1,by = 0.01))
  model11 <- train(status~., data=inTrain, method='glmnet',metric='Accuracy', tuneGrid=tunegrid, trControl=control, probability = TRUE )
  model12 <- train(status ~ ., data=inTrain, method='glmnet',probability = TRUE )
  
  control <- trainControl(method="repeatedcv",repeats = 3)
  model21 <- train(status~., data=inTrain, method = "knn", trControl = control, preProcess = c("center","scale"), tuneLength = 20)
  # model22 <- train(status ~ ., data=inTrain, method='knn', preProcess = c("center","scale"), probability = TRUE )
  
  control <- trainControl(method='repeatedcv', repeats=3, search='grid')
  tunegrid <- expand.grid(.mtry = c(1:sqrt(ncol(inTrain)-1)))
  model31 <- train(status ~ ., inTrain, method='rf', metric='Accuracy', tuneGrid=tunegrid, trControl=control, probability = TRUE)
  model32 <- train(status ~ ., data=inTrain, method='rf', probability = TRUE )
  
  control <- trainControl(method='repeatedcv', number=5, repeats=3, search='grid')
  tunegrid <- expand.grid(.cost=seq(0.1,2,0.1))
  model41 <- train(status ~ ., inTrain, method='svmLinear2', metric='Accuracy', tuneGrid=tunegrid, trControl=control, probability = TRUE)
  model42 <- train(status ~ ., inTrain, method='svmLinear2', probability=TRUE)
  
  res11_train <- predict(model11, newdata=as.matrix(inTrain[,-1]), type = "prob"); rownames(res11_train) <- rownames(inTrain)
  res21_train <- predict(model21, newdata=as.matrix(inTrain[,-1]), type = "prob"); rownames(res21_train) <- rownames(inTrain)
  res31_train <- predict(model31, newdata=as.matrix(inTrain[,-1]), type = "prob"); rownames(res31_train) <- rownames(inTrain)
  res41_train <- predict(model41, newdata=as.matrix(inTrain[,-1]), type = "prob"); rownames(res41_train) <- rownames(inTrain)
  res12_train <- predict(model12, newdata=as.matrix(inTrain[,-1]), type = "prob"); rownames(res12_train) <- rownames(inTrain)
  res32_train <- predict(model32, newdata=as.matrix(inTrain[,-1]), type = "prob"); rownames(res32_train) <- rownames(inTrain)
  res42_train <- predict(model42, newdata=as.matrix(inTrain[,-1]), type = "prob"); rownames(res42_train) <- rownames(inTrain)
  
  
  res11_test <- predict(model11, newdata=as.matrix(inTest[,-1]), type = "prob"); rownames(res11_test) <- rownames(inTest)
  res21_test <- predict(model21, newdata=as.matrix(inTest[,-1]), type = "prob"); rownames(res21_test) <- rownames(inTest)
  res31_test <- predict(model31, newdata=as.matrix(inTest[,-1]), type = "prob"); rownames(res31_test) <- rownames(inTest)
  res41_test <- predict(model41, newdata=as.matrix(inTest[,-1]), type = "prob"); rownames(res41_test) <- rownames(inTest)
  res12_test <- predict(model12, newdata=as.matrix(inTest[,-1]), type = "prob"); rownames(res12_test) <- rownames(inTest)
  res32_test <- predict(model32, newdata=as.matrix(inTest[,-1]), type = "prob"); rownames(res32_test) <- rownames(inTest)
  res42_test <- predict(model42, newdata=as.matrix(inTest[,-1]), type = "prob"); rownames(res42_test) <- rownames(inTest)
  
  
  inTrainfinal <- data.frame(smp=rownames(inTrain),status=inTrain[,1]) %>%
    merge(res11_train, by.x="smp", by.y=0) %>%
    merge(res21_train, by.x="smp", by.y=0) %>%
    merge(res31_train, by.x="smp", by.y=0) %>%
    merge(res41_train, by.x="smp", by.y=0) %>%
    merge(res12_train, by.x="smp", by.y=0) %>%
    merge(res32_train, by.x="smp", by.y=0) %>%
    merge(res42_train, by.x="smp", by.y=0) 
  rownames(inTrainfinal) <- inTrainfinal[,1]; inTrainfinal[,1] <- NULL
  inTestfinal <- data.frame(smp=rownames(inTest),status=inTest[,1]) %>%
    merge(res11_test, by.x="smp", by.y=0) %>%
    merge(res21_test, by.x="smp", by.y=0) %>%
    merge(res31_test, by.x="smp", by.y=0) %>%
    merge(res41_test, by.x="smp", by.y=0) %>%
    merge(res12_test, by.x="smp", by.y=0) %>%
    merge(res32_test, by.x="smp", by.y=0) %>%
    merge(res42_test, by.x="smp", by.y=0) 
  rownames(inTestfinal) <- inTestfinal[,1]; inTestfinal[,1] <- NULL
  colnames(inTrainfinal)[-1] <- c(str_c("prob",seq(1:(ncol(inTrainfinal)-1))))
  colnames(inTestfinal)[-1] <- c(str_c("prob",seq(1:(ncol(inTestfinal)-1))))
  
  model <- train(status~., data=inTrainfinal,
                 method=mlmodel,
                 probability = TRUE )
  res_test <- predict(model, newdata=as.matrix(inTestfinal[,-1]), type = "prob")
  res_train <- predict(model, newdata=as.matrix(inTrainfinal[,-1]), type = "prob")
  
  res <- list("pred_test"=c(rownames(inTest),inTest[,1],
                            round(as.numeric(res_test),2),as.character(predict(model, newdata = as.matrix(inTestfinal[,-1]))),
                            round(as.numeric(res11_test),2),as.character(predict(model11, newdata = as.matrix(inTest[,-1]))),
                            round(as.numeric(res21_test),2),as.character(predict(model21, newdata = as.matrix(inTest[,-1]))),
                            round(as.numeric(res31_test),2),as.character(predict(model31, newdata = as.matrix(inTest[,-1]))),
                            round(as.numeric(res41_test),2),as.character(predict(model41, newdata = as.matrix(inTest[,-1]))),
                            round(as.numeric(res12_test),2),as.character(predict(model12, newdata = as.matrix(inTest[,-1]))),
                            round(as.numeric(res32_test),2),as.character(predict(model32, newdata = as.matrix(inTest[,-1]))),
                            round(as.numeric(res42_test),2),as.character(predict(model42, newdata = as.matrix(inTest[,-1])))),
              "smp" = list(rownames(inTrain)),
              "cmb" = round(res_train,2),
              "glm_cv"=round(res11_train,2),
              "knn_cv"=round(res21_train,2),
              "rf_cv"=round(res31_train,2),
              "svm_cv"=round(res41_train,2),
              "glm"=round(res12_train,2),
              "rf"=round(res32_train,2),
              "svm"=round(res42_train,2))
  return(res)
}

DmrPrediction <- function(mat, trainsmp, testsmp, pvalue=0.01, diffmeth=0.05, ndmr1, ndmr2, ndmr3){
  # Use DMR for prediction
  #
  # Args:
  #   mat:        input_matrix
  #   label:      cancer type("HCC" or "PDAC")
  #   trainsmp:   smp_id in train
  #   testsmp:    smp_id in test
  #   addsmp:     smp_id in additional test set
  
  # Returns:
  #   res:       summary of predictions
  
  
  inTrain <- mat[ rownames(mat) %in% trainsmp, ]
  inTest  <- mat[ rownames(mat) %in% testsmp, ]
  
  idx1 <- which(inTrain$status=="Healthy")
  idx2 <- which(inTrain$status=="PDAC")
  idx3 <- which(inTrain$status=="HCC")
  
  ### find dmr ###
  #### one vs. one
  dmr_t2 <- function(x1,x2, diffmeth=diffmeth) {
    if(abs(mean(x1)-mean(x2)) <= diffmeth ){
      diffres <- 1
    }else{
      diffres <- t.test(x1, x2)$p.value
    }
    return(diffres)
  }
  
  dmr_p12 <- apply(inTrain[, -1], 2, function(x){dmr_t2(x[idx1], x[idx2], diffmeth=diffmeth)}) %>% as.numeric()
  dmr_p13 <- apply(inTrain[, -1], 2, function(x){dmr_t2(x[idx1], x[idx3], diffmeth=diffmeth)}) %>% as.numeric()
  dmr_p23 <- apply(inTrain[, -1], 2, function(x){dmr_t2(x[idx2], x[idx3], diffmeth=diffmeth)}) %>% as.numeric()
  
  
  cat("pvalues:", dmr_p12[order(dmr_p12)][ndmr1], dmr_p13[order(dmr_p13)][ndmr2], dmr_p23[order(dmr_p23)[ndmr3]],"\n")
  dmr_idx  <- unique(c(order(dmr_p12)[1:ndmr1], order(dmr_p13)[1:ndmr2], order(dmr_p23)[1:ndmr3])) + 1
  ### dealing with overlap regions ###
  # cat("number of dmr:",length(dmr_idx),"\n")
  # selidx <- str_split_fixed(colnames(inTrain)[dmr_idx],"_",4)[,1:3] %>% as.data.frame(stringsAsFactors=FALSE)
  # selidx[,-1] <- lapply(selidx[,-1],as.numeric)
  # colnames(selidx) <- c("chr","start","end")
  # seldmr <- makeGRangesFromDataFrame(selidx)
  # seldmr_merge <- findOverlaps(reduce(seldmr),seldmr) %>% as.data.frame(stringsAsFactors=FALSE)
  # seldmr_uniq <- seldmr_merge %>%
  #   group_by(queryHits) %>%
  #   filter(subjectHits == max(subjectHits)) %>%
  #   distinct %>% as.data.frame(stringsAsFactors=FALSE)
  # dmr_idx_uniq <- dmr_idx[sort(seldmr_uniq$subjectHits)]
  # cat("number of non-overlap dmr:",length(dmr_idx_uniq),"\n")
  # inTrain <- inTrain[, c(1, dmr_idx_uniq)]; inTest <- inTest[, c(1, dmr_idx_uniq)]
  ### not dealing with overlap ###
  inTrain <- inTrain[, c(1, dmr_idx)]; inTest <- inTest[, c(1, dmr_idx)]
  # pheatmap(inTrain[,-1], show_colnames = FALSE, cluster_rows = FALSE)
  # pca_res <- prcomp(t(inTrain[,-1]), scale. = FALSE)
  # ggplot(as.data.frame(pca_res$rotation), aes(x=PC1,y=PC2, color=inTrain[,1], label=rownames(inTrain))) + geom_point() + geom_text(aes(label=rownames(inTrain)),hjust=0, vjust=0)
  # 
  ### summarize dmr ###
  cat("\n",table(gsub("chr.*","chr",colnames(inTrain)[-1])),"\n")
  cat("\nlength:",as.numeric(quantile(as.numeric(str_split_fixed(gsub(".*chr","chr",colnames(inTrain)[-1]) ,"_",4)[,3]) -
                                        as.numeric(str_split_fixed(gsub(".*chr","chr",colnames(inTrain)[-1]) ,"_",4)[,2]))),"\n")
  
  cat("healthy-pdac\n\tndmr1: ",ndmr1,"\n\tdiff:", quantile(as.numeric(abs(apply(inTrain[idx1,colnames(inTrain)%in%colnames(mat)[order(dmr_p12)[1:ndmr1]+1]],2,mean) - apply(inTrain[idx2,colnames(inTrain)%in%colnames(mat)[order(dmr_p12)[1:ndmr1]+1]],2,mean)))),"\n")
  cat("healthy-hcc\n\tndmr2: ",ndmr2,"\n\tdiff:",  quantile(as.numeric(abs(apply(inTrain[idx1,colnames(inTrain)%in%colnames(mat)[order(dmr_p13)[1:ndmr1]+1]],2,mean) - apply(inTrain[idx3,colnames(inTrain)%in%colnames(mat)[order(dmr_p13)[1:ndmr1]+1]],2,mean)))),"\n")
  cat("hcc-pdac\n\tndmr3: ",ndmr3,"\n\tdiff:", quantile(as.numeric(abs(apply(inTrain[idx2,colnames(inTrain)%in%colnames(mat)[order(dmr_p23)[1:ndmr1]+1]],2,mean) - apply(inTrain[idx3,colnames(inTrain)%in%colnames(mat)[order(dmr_p23)[1:ndmr1]+1]],2,mean)))),"\n")
  
  ### define train & test ###
  res <- mlmethods(inTrain=inTrain, inTest=inTest)
  cat(res$pred_test,"\n")
  res$dmrs_pos <- colnames(inTrain)[-1]
  res$intrain <- inTrain
  res$intest <- inTest
  return(res)
}

parseArgs <- function(args, manditories) {
  if(length(args) %% 2 == 1 || length(args) == 0) {
    cat(args,"\n")
    cat('Unpaired argument and value.\n')
    return(NULL)
  }else(
    cat(length(args),"\n")
  )
  n.i <- seq(1, length(args), by=2)
  v.i <- seq(2, length(args), by=2)
  args.name <- args[n.i]
  args.value <- args[v.i]
  
  # Check if required argument values are supplied.
  miss_tag <- F
  man.bool <- manditories %in% args.name
  if(!all(man.bool)){
    cat(paste('Missing argument: ', paste(manditories[!man.bool], 
                                          collapse=','), '.', sep='')
    )
    miss_tag <- T
  }
  if(miss_tag){
    res <- NULL
  }else{
    res <- args.value
    names(res) <- args.name
  }
  res
}

cmd.help <- function(){
  cat("\nUsage:  -m methylation -o outfix -n ndmr \ne.g.\n")
  cat("\n\t -m GRCh38.Regulatory_Build.promoter.agg.count.cpgnum.depth2.ratio.txt \n")
  cat("\t -o GRCh38.Regulatory_Build.promoter.agg.count.cpgnum.depth2.ratio.loo.txt -n 1000 \n")
}



# Input argument parser.
args <- commandArgs(T)
args.tbl <- parseArgs(args, c('-m', '-s', '-o', '-n', '-d'))
if(is.null(args.tbl)){
  cmd.help()
  stop('Error in parsing command line arguments. Stop.\n')
}

methFile <- as.character(args.tbl['-m'])   
splitFile <- as.character(args.tbl['-s'])   
outfix <- as.character(args.tbl['-o'])
ndmrs <- as.numeric(str_split_fixed(as.character(args.tbl['-n']),"_",3))
diffmeth <- as.numeric(args.tbl['-d'])
##### test #####
# setwd("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/results/meth_correction/exclude_cured")
# methFile <- c("filtered.Regulatory_Build.selsmp.ws1000.agg.count.cpgnum.depth2.ratio.txt")
# splitFile <- c("/users/ludwig/cfo155/cfo155/cfDNA/022020_cfDNA/resource/cfDNA_cohort_1_sample_list_11302020.excured.regroup.txt")
# ndmrs <- as.numeric(str_split_fixed("5_5_5","_",3))
# diffmeth<-0

cat("methFile:\n\t",  methFile,"\n")

##### transpose dat #####
options(scipen = 999)
dat <- read_delim(methFile, delim = "\t", col_names = TRUE) %>% as.data.frame(stringsAsFactors=FALSE)
dat$pos_cpg <- gsub(".*chr","chr",dat$pos_cpg)
dat$pos_cpg <- paste(str_split_fixed(dat$pos_cpg,"_",4)[,1], as.numeric(str_split_fixed(dat$pos_cpg,"_",4)[,2]), as.numeric(str_split_fixed(dat$pos_cpg,"_",4)[,3]), str_split_fixed(dat$pos_cpg,"_",4)[,4],sep="_")
dat.t <- dat[,-1] %>% t() %>% as.data.frame(stringsAsFactors=FALSE)
colnames(dat.t) <- dat$pos_cpg
rownames(dat.t) <- gsub("_rC","",rownames(dat.t))
dat.t <- cbind(status=as.character(str_split_fixed(rownames(dat.t), "_", 2)[,1]), dat.t)
dat.t$status <- as.character(dat.t$status)
head(dat.t[1:4,1:4])

##### run prediction #####
smpstatus <- read_delim(splitFile, delim = "\t", col_names = TRUE) %>% as.data.frame(stringsAsFactors=FALSE)
smpstatus <- smpstatus[smpstatus$Status%in% c("Ctrl","HCC","PDAC"),c(10,6)]
allSmp <- smpstatus$idx
dat.t <- merge(smpstatus,dat.t,by.x="idx",by.y=0)[,-3]
rownames(dat.t) <- dat.t$idx
colnames(dat.t)[2] <- "status"
dat.t[,1]<-NULL
dat.t$status <- gsub("Ctrl","Healthy",dat.t$status)


res = list()
for(i in 1:length(allSmp)){
  trainSmp <- as.character(allSmp[-i])
  testSmp <- as.character(allSmp[i])
  ratio_mat <- dat.t[rownames(dat.t) %in% c(trainSmp,testSmp),]
  cat("Train:\n\t",trainSmp,"\nTest:\n\t",testSmp,"\n")
  cat("N:\t",i,"/", length(allSmp),"\n")
  res[[i]] <- DmrPrediction(mat = ratio_mat,
                            trainsmp = trainSmp,
                            testsmp = testSmp,
                            pvalue=0.05, diffmeth=diffmeth, 
                            ndmr1 = ndmrs[1], ndmr2 = ndmrs[2], ndmr3 = ndmrs[3]
                            )
}


scores <- do.call(rbind,do.call(rbind, res)[,1]) %>% as.data.frame(stringsAsFactors=FALSE)
colnames(scores) <- c("smp","status",
                      "cmb_rf_hcc","cmb_rf_healthy","cmb_rf_pdac","cmb_predictted",
                      "glm_cv_hcc","glm_cv_healthy","glm_cv_pdac","glm_cv_predictted",
                      "knn_cv_hcc","knn_cv_healthy","knn_cv_pdac","knn_cv_predictted",
                      "rf_cv_hcc","rf_cv_healthy","rf_cv_pdac","rf_cv_predictted",
                      "svm_cv_hcc","svm_cv_healthy","svm_cv_pdac","svm_cv_predictted",
                      "glm_hcc","glm_healthy","glm_pdac","glm_predictted",
                      "rf_hcc","rf_healthy","rf_pdac","rf_predictted",
                      "svm_hcc","svm_healthy","svm_pdac","svm_predictted")

write.table(scores, outfix, col.names=TRUE, row.names=FALSE, quote=FALSE, sep="\t")
save(res, file = gsub(".txt",".RData",outfix))
dmrs <- do.call(cbind, res)[11,] %>%  unlist() %>% table() %>% as.data.frame()
dmrs <- cbind(str_split_fixed(dmrs$.,"_",4)[,1:3],dmrs$Freq) %>% as.data.frame()
write.table(dmrs, gsub(".txt",".dmr.bed", outfix), col.names=FALSE, row.names=FALSE, quote=FALSE, sep="\t")

