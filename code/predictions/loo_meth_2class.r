#!/mnt/lustre/users/jcheng/tools/miniconda2/bin/Rscript --vanilla

# Specify a job name
#$ -N dmrpredict

# Run the job in the current working directory
#$ -cwd -j y -V
#$ -l h_rt=80:00:00
#$ -l h_vmem=2G
# Parallel environemnt settings
#$ -pe smp 1
# Some useful data about the job to help with debugging

.libPaths(c(.libPaths(),"/users/jcheng/R/x86_64-pc-linux-gnu-library/3.6","/mnt/lustre/users/jcheng/tools/miniconda2/lib/R/library","/users/jcheng/gridware/share/R/3.6.2"))


library(readr)
library(tidyverse)
library(reshape2)
library(dplyr)
library(caret)
library(glmnet)
library(ROCR)
library(ggplot2)
library(pryr)
library(cowplot)
library(ggplotify)
library(RColorBrewer)
library(MLmetrics)
library(GenomicRanges)
library(plotROC)



source("code/functions.r")
cmd.help <- function(){
  cat("\nUsage:  -m methylation -s splits_file -c cancer_type -a added_group -o outfix -n pval\ne.g.\n")
  cat("\n\t -m filtered.promoter.enhancer.selsmp.ws1000.agg.count.cpgnum.depth2.ratio.txt \n")
  cat("\t -s resource/mlsplits/HCC_allHealthy.splits.txt \n")
  cat("\t -c HCC \n")
  cat("\t -a None(choose from None;Cirrhosis;Pancreatitis;Healthyinflammation)\n")
  cat("\t -o CGI.TF.HCC_allHealthy.splits.txt -n 500 \n")
}



# Input argument parser.
args <- commandArgs(T)
args.tbl <- parseArgs(args, c('-m', '-s', '-c', '-a', '-o', '-p'))
if(is.null(args.tbl)){
  cmd.help()
  stop('Error in parsing command line arguments. Stop.\n')
}

methFile <- as.character(args.tbl['-m'])   
splitFile <- as.character(args.tbl['-s'])
cancer <- as.character(args.tbl['-c'])
addtest <- as.character(args.tbl['-a'])
outfix <- as.character(args.tbl['-o'])
pval <- as.numeric(args.tbl['-p'])

options(scipen=999)
DmrPrediction <- function(mat, label, trainsmp, testsmp, addsmp, outfix, pvalue=0.01, diffmeth=0.05){
  # Use DMR for prediction
  #
  # Args:
  #   mat:        input_matrix
  #   label:      cancer type("HCC" or "PDAC")
  #   trainsmp:   smp_id in train
  #   testsmp:    smp_id in test
  #   addsmp:     smp_id in additional test set
  
  # Returns:
  #   res:       summary of predictions
  set.seed(202010)
  mat$status[mat$status!=label] <- "ctrl"
  inTrain <- mat[ rownames(mat) %in% trainsmp, ]
  inTest  <- mat[ rownames(mat) %in% c(testsmp,addsmp), ]
  cat("testintrain:", rownames(inTest) %in% rownames(inTrain),"\n")
  idx1 <- which(inTrain$status==label)
  idx2 <- which(inTrain$status!=label)
  ### find dmr ###
  # filter by min diff #
  dmr_diff <- apply(inTrain[, -1], 2 , function(x) {x <- mean(x[idx1]) - mean(x[idx2])}) %>% as.numeric()
  dmr_idx <- which(abs(dmr_diff) > diffmeth) +1
  inTrain <- inTrain[, c(1,dmr_idx)]; inTest  <- inTest[, c(1,dmr_idx)]
  # filter by p-value #
  dmr_p <- apply(inTrain[, -1], 2, function(x){t.test(x[idx1], x[idx2])$p.value}) %>% as.numeric()
  ## choose ndmr / p-value in total ##
  dmr_idx  <- which(dmr_p < pvalue)+1
  ### length of dmr ###
  inTrain <- inTrain[, c(1, dmr_idx)]; inTest <- inTest[, c(1, dmr_idx)]
  cat("\n",table(gsub("chr.*","chr",colnames(inTrain)[-1])),"\n")
  cat("\nlength:",as.numeric(quantile(as.numeric(str_split_fixed(gsub(".*chr","chr",colnames(inTrain)[-1]) ,"_",4)[,3]) -
                                        as.numeric(str_split_fixed(gsub(".*chr","chr",colnames(inTrain)[-1]) ,"_",4)[,2]))),"\n")
  
  ### dealing with overlap regions ###
  cat("number of dmr:",length(dmr_idx),"\n")
  selidx <- str_split_fixed(gsub(".*chr","chr",colnames(inTrain)[-1]),"_",4)[,1:3] %>% as.data.frame(stringsAsFactors=FALSE)
  selidx[,-1] <- lapply(selidx[,-1],as.numeric)
  colnames(selidx) <- c("chr","start","end")
  
  seldmr <- makeGRangesFromDataFrame(selidx)
  seldmr_merge <- findOverlaps(reduce(seldmr),seldmr) %>% as.data.frame(stringsAsFactors=FALSE)
  seldmr_uniq <- seldmr_merge %>%
    group_by(queryHits) %>%
    filter(subjectHits == max(subjectHits)) %>%
    distinct %>% as.data.frame(stringsAsFactors=FALSE)
  dmr_idx_uniq <- unique(sort(seldmr_uniq$subjectHits)) +1 
  cat("number of non-overlap dmr:",length(dmr_idx_uniq),"\n")
  inTrain <- inTrain[, c(1, dmr_idx_uniq )]; inTest <- inTest[, c(1, dmr_idx_uniq)]
  ### define train & test ###
  real_label_train <- as.numeric(inTrain$status==label) %>% as.factor()
  real_label_test  <- as.numeric(inTest$status==label)  %>% as.factor()
  
  ### build model ##
  # cv.glmnet #
  ob1 <- cv.glmnet(x=as.matrix(inTrain[,-1]),
                   y=as.numeric(inTrain$status == label),
                   family="binomial", alpha=1, 
                   nfolds = 5, lambda = seq(0.01,0.1,by = 0.01), 
                   standardize=TRUE)
  
  # saveRDS(ob1, paste0(outfix,'.model.',testsmp,".rds"))
  pred_train1 <- predict(ob1, newx=as.matrix(inTrain[,-1]), type = "response", s = "lambda.min")
  pred_test1  <- predict(ob1, newx=as.matrix(inTest[,-1]), type = "response", s = "lambda.min") %>% as.numeric()
  names(pred_test1) <- rownames(inTest)
  
  pred_label_test1  <- factor(predict(ob1, newx=as.matrix(inTest[,-1]), type="class"),levels=c(0,1))
  AUC_train1 <- AUC(pred_train1, real_label_train)
  cat("pred_test_cvglm = ",names(pred_test1),"\n")
  cat("pred_test_cvglm = ",pred_test1,"\n")
  cat("auc_cvglm_train = ", AUC_train1,"\n" )
  res <- list("smp" = testsmp, "pred_test_cvglm" = pred_test1,
              "label_test_cvglm" = pred_label_test1,
              "auc_cvglm_train" = AUC_train1,
              "dmr"=colnames(inTrain)[-1])
  return(res)
}

cat("methFile:\n\t",  methFile, 
    "\nsplits:\n\t",  splitFile,
    "\ncancer:\n\t",  cancer,
    "\naddtest:\n\t", addtest,
    "\noutfix:\n\t",  outfix,"\n")

##### transpose dat #####
dat <- read_delim(methFile, delim = "\t", col_names = TRUE) %>% as.data.frame(stringsAsFactors=FALSE)
dat.t <- dat[,-1] %>% t() %>% as.data.frame(stringsAsFactors=FALSE)
colnames(dat.t) <- dat$pos_cpg
rownames(dat.t) <- gsub("_rC","",rownames(dat.t))
dat.t <- cbind(status=as.character(str_split_fixed(rownames(dat.t), "_", 2)[,1]), dat.t)
dat.t$status <- as.character(dat.t$status)
head(dat.t[1:4,1:4])

##### run prediction #####

splits <- read.table(splitFile,header=TRUE,sep="\t",stringsAsFactors = FALSE)
addSmp <- rownames(dat.t)[dat.t$status %in% c(str_split_fixed(addtest,";",str_count(addtest,";")+1))]
smpstatus <- data.frame(smp=rownames(dat.t),status=dat.t$status)
allSmp <- splits[splits$Status %in% c("Ctrl",cancer),10]

res = list()
for(i in 1:length(allSmp)){
  trainSmp <- allSmp[-i]
  testSmp <- allSmp[i]
  ratio_mat <- dat.t[rownames(dat.t) %in% c(trainSmp,testSmp,addSmp),]
  cat("Train:\n\t",trainSmp,"\nTest:\n\t",testSmp,"\nAdd:\n\t",addSmp,"\n")
  cat("N:\t",i,"/", length(allSmp),"\n")
  res[[i]] <- DmrPrediction(mat = ratio_mat,
                            label = cancer,
                            trainsmp = trainSmp,
                            testsmp = testSmp,
                            addsmp = addSmp,
                            pvalue=pval,
                            outfix=outfix)
}


scores <- do.call(cbind, res)[2,] %>% unlist() 
scores <- data.frame(as.list(scores)) %>% t() %>% as.data.frame(stringsAsFactors=FALSE)
colnames(scores) <- "pred_test_cvglm"
scores$smp <- gsub("\\.[0-9]*","",rownames(scores))

scores <- merge(scores, smpstatus, by=c("smp"),all.x=TRUE)
write.table(scores,paste0(outfix,".ori.score.txt"),col.names=TRUE, row.names=FALSE, quote=FALSE, sep="\t")
scores <- aggregate(scores$pred_test_cvglm, by=list(scores$status,scores$smp), FUN=mean)
colnames(scores) <- c("status","smp","pred_test_cvglm")
write.table(scores,paste0(outfix,".score.txt"),col.names=TRUE, row.names=FALSE, quote=FALSE, sep="\t")


cat("\n",splitFile,"\nAUC:\n",AUC(scores[scores$status!=addtest,]$pred_test_cvglm, as.numeric(scores[scores$status!=addtest,]$status==cancer)),"\n")


dmr <- str_split_fixed(gsub(".*chr","chr",do.call(cbind, res)[5,] %>% unlist() %>% unique()),"_",4)[,1:3] %>% as.data.frame(stringsAsFactors=FALSE)
write.table(dmr,paste0(outfix,".dmr.txt"),col.names=TRUE, row.names=FALSE, quote=FALSE, sep="\t")

if(addtest!="None"){
  fac <- with(scores, reorder(smp, pred_test_cvglm, median, order = TRUE))
  scores$smp <- factor(scores$smp, levels = levels(fac))
  p1 <- ggplot(scores, aes(x=smp, y=pred_test_cvglm, color=status)) +
    geom_point() +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1),legend.position="bottom") +
    scale_color_brewer(palette = "Dark2")
  p2 <- ggplot(scores[scores$status!=addtest,], aes(x=smp, y=pred_test_cvglm, color=status)) +
    geom_point() +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1),legend.position="bottom") +
    scale_color_brewer(palette = "Dark2")
  p <- plot_grid(p1,p2, ncol = 1, rel_heights = c(0.5, 0.5))
  ggsave(paste0(outfix,".prob.png"), p, height=10,width=10)



  scores$truth <- as.numeric(scores$status==cancer)
  mlauc1 <- round(AUC(scores[scores$status!=addtest,]$pred_test_cvglm, scores[scores$status!=addtest,]$truth),2)
  mlauc2 <- round(AUC(scores$pred_test_cvglm, scores$truth),2)

  p <- ggplot() +
    geom_roc(data=scores[scores$status!=addtest,], aes(d = truth, m = pred_test_cvglm), labels = FALSE, color="#8F7700FF") +
    geom_roc(data=scores, aes(d = truth, m = pred_test_cvglm), labels = FALSE, color="#EFC000FF") +
    theme_classic() +
    annotate(geom="text", x=0.3, y=0.6, label=paste0(cancer," AUC: ",mlauc1),color="#8F7700FF",size=5) +
    annotate(geom="text", x=0.3, y=0.4, label=paste0(cancer,"+",addtest," AUC: ",mlauc2),color="#EFC000FF",size=5) +
    ggtitle(outfix)+
    theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
          panel.background = element_blank(), axis.line = element_line(colour = "black"))
  ggsave(paste0(outfix,"cvglm.roc.png"), p, height=5, width=5)

}else{
  fac <- with(scores, reorder(smp, pred_test_cvglm, median, order = TRUE))
  scores$smp <- factor(scores$smp, levels = levels(fac))
  p <- ggplot(scores, aes(x=smp, y=pred_test_cvglm, color=status)) +
    geom_point() +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1),legend.position="bottom") +
    scale_color_brewer(palette = "Dark2")

  ggsave(paste0(outfix,".prob.png"), p, height=5,width=10)



  scores$truth <- as.numeric(scores$status==cancer)
  mlauc1 <- round(AUC(scores$pred_test_cvglm, scores$truth),2)

  p <- ggplot() +
    geom_roc(data=scores, aes(d = truth, m = pred_test_cvglm), labels = FALSE, color="#EFC000FF") +
    theme_classic() +
    annotate(geom="text", x=0.3, y=0.6, label=paste0(cancer," AUC: ",mlauc1),color="#8F7700FF",size=5) +
    ggtitle(outfix)+
    theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
          panel.background = element_blank(), axis.line = element_line(colour = "black"))
  ggsave(paste0(outfix,"cvglm.roc.png"), p, height=5, width=5)

}
