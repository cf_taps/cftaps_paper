## 0. Raw data
Cohort description:
**85** samples were left for further analysis.  
well_ox_Jan, well_ox, novogene

|Status | Filtered|
|---    |---      |
|Healthy| 30      |
|HCC    | 21      |
|PDAC   | 23      |
|Cirrhosis|4      |
|Pancreatitis| 7  |


## 1. File description
```bash
# Processed files
## Bam: 
/gpfs2/well/ludwig/users/cfo155/cfDNA/022020_cfDNA/align/merge/*_*bam  
## Methy call:
/gpfs2/well/ludwig/users/cfo155/cfDNA/022020_cfDNA/meth_methyldackel/bulk_v1/*_*.sort.md.clip.10.140.75.75_CpG.bedGraph.gz

# Aggregated files
## All CpG sites:
/gpfs2/well/ludwig/users/cfo155/cfDNA/022020_cfDNA/meth_methyldackel/bulk_v1/CpG_meth_count.clip.10.140.75.75.filtered.txt
## Enhancer:
/gpfs2/well/ludwig/users/cfo155/cfDNA/022020_cfDNA/results/meth_correction/matrix/enhancer.count.agg.count.cpgnum.depth2.ratio.txt
## Promoter:
/gpfs2/well/ludwig/users/cfo155/cfDNA/022020_cfDNA/results/meth_correction/matrix/promoter.count.agg.count.cpgnum.depth2.ratio.txt
## 1kb bins:
/gpfs2/well/ludwig/users/cfo155/cfDNA/022020_cfDNA/results/meth_correction/matrix/filtered.selsmp.ws1000.agg.count.cpgnum.depth2.ratio.txt
##
################################### Original ################################### 
/users/ludwig/cfo155/cfo155/cfDNA/cfDNA_clean/cftaps_paper/data
cp /gpfs2/well/ludwig/users/cfo155/cfDNA/022020_cfDNA/results/meth_correction/matrix/filtered.selsmp.ws1000.agg.count.cpgnum.depth2.ratio.txt ./
```

## 2. Code

### 2.1 Preprocess
* All Three runs were pre-processed seperately:  
Trim -> Align -> MarkDuplicates  
* Alignment files of the same sample were merge and used to call methylation:  
Merge -> MarkDuplicates -> MethylDackel
* Statistics:   
mapping rate and conversion

```bash
################################### Original ###################################
01.trimreads.sh # trim_galore
02.align.sh     # bwa mem
03.filterbam.sh # picard MarkDuplicates
04.call_methyDackel.sh     # MethylDackel
05.picard_collectmetrix.sh # CollectMultipleMetrics
08.conversion.sh           # caculate conversion
rename_fastq.sh            # merge 3 runs
################################### Clean ######################################

```

### 2.2 Main Figure
``` bash
################################### Clean ######################################
# Figure 1
/gpfs2/well/ludwig/users/cfo155/cfDNA/cfDNA_clean/cftaps_paper/code/figs/fig1.r
# Figure 2
/gpfs2/well/ludwig/users/cfo155/cfDNA/cfDNA_clean/cftaps_paper/code/figs/fig2.r
# Figure 3
/gpfs2/well/ludwig/users/cfo155/cfDNA/cfDNA_clean/cftaps_paper/code/figs/fig3.r
# Figure 4
/gpfs2/well/ludwig/users/cfo155/cfDNA/cfDNA_clean/cftaps_paper/code/figs/fig4.r
```

### 2.3 Supp Figure
```bash
################################### Clean ######################################
# Figure S1
/gpfs2/well/ludwig/users/cfo155/cfDNA/cfDNA_clean/cftaps_paper/code/figs/figs1.r
# Figure S2
/gpfs2/well/ludwig/users/cfo155/cfDNA/cfDNA_clean/cftaps_paper/code/figs/S2_figure.r
# Figure S3
/gpfs2/well/ludwig/users/cfo155/cfDNA/cfDNA_clean/cftaps_paper/code/figs/fig2.r
/gpfs2/well/ludwig/users/cfo155/cfDNA/cfDNA_clean/cftaps_paper/code/figs/S2_figure.r
# Figure S4
/gpfs2/well/ludwig/users/cfo155/cfDNA/cfDNA_clean/cftaps_paper/code/figs/fig3.r
# Figure S5
/gpfs2/well/ludwig/users/cfo155/cfDNA/cfDNA_clean/cftaps_paper/code/figs/cnaclinic.r
# Figure S5
/gpfs2/well/ludwig/users/cfo155/cfDNA/cfDNA_clean/cftaps_paper/code/figs/fig3.r
# Figure S7
/gpfs2/well/ludwig/users/cfo155/cfDNA/cfDNA_clean/cftaps_paper/code/figs/fig4.r
```
